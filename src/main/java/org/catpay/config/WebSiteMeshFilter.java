package org.catpay.config;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.stereotype.Component;

@Component
public class WebSiteMeshFilter extends ConfigurableSiteMeshFilter{
	
	@Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        builder.addDecoratorPath("/*", "/modules/common/decorator").addExcludedPath("/login.html").
        addExcludedPath("/modules/index.html")
        .addExcludedPath("/static/**")
        .addExcludedPath("/swagger-ui.html");
    }


}
