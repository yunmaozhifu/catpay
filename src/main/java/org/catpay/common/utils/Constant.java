package org.catpay.common.utils;

/**
 * 常量
 * 
 */
public class Constant {
	
	/** 超级管理员ID */
	public static final int SUPER_ADMIN = 1;
	
	public static final int ADMIN_GROUP = 1;
	
	public static final String CONFIG_KEY = "sys:config:";
	
	
	public static final String SESSION_ID ="sessionid";
	
	public static final String ORIGINAL_PASSWORD = "123456";
	
	public static final Integer [] IGNORE_MENU= {2,4,5};

	/**
	 * 菜单类型
	 * 
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * 定时任务状态
     * 
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 暂停
         */
    	PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum WskMSType {
        /**
         * 点对点
         */
        PTP(1),
        /**
         * 点对组
         */
        PTG(2);
    

        private int value;

    	WskMSType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    public enum WskMSBuss {
        /**
         * 聊天
         */
        chat(1),
        /**
         * 服务
         */
        service(2),
        /**
         * 提醒
         */
        notice(3);
    

        private int value;

        WskMSBuss(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
