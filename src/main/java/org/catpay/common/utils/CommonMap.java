package org.catpay.common.utils;

import java.util.HashMap;

public class CommonMap extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6848094014094039126L;

	@Override
	public CommonMap put(String key, Object value) {
		 super.put(key, value);
		 return this;
	}

	public static CommonMap getMap() {
		return new CommonMap();
	}
	
}
