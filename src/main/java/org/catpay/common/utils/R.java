package org.catpay.common.utils;

import org.apache.http.HttpStatus;

import lombok.Data;

/**
 * 返回数据
 * @param <T>
 * 
 */
@Data
public class R<T> {
	
	private String code;
	private String message;
    private T result;
	
	public R() {
		this.setCode("0000");
		this.setMessage(message);
	}
	
	public static R<?> error() {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR+"", "系统异常，请联系管理员");
	}
	
	public static R<?> error(String msg) {
		return error(HttpStatus.SC_NOT_ACCEPTABLE+"", msg);
	}
	
	public static R<?> error(String code, String msg) {
		R<?> r = new R<Object>();
		r.setCode(code);
	    r.setMessage(msg);

		return r;
	}

	public static R<?> ok(String msg) {
		R<?> r = new R<Object>();
		r.setMessage(msg);
		return r;
	}
	
	public static <T> R<T> ok(T result) {
		R<T> r = new R<T>();
		r.setResult(result);
		return r;
	}
	
	public static R<?> ok() {
		return new R<Object>();
	}

}
