package org.catpay.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 自定义异常
 * 
 */
public class RRException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	@Getter 
	@Setter
    private String msg;
	
	@Getter 
	@Setter
    private int code = 406;
    
    public RRException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	public RRException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}
	
	public RRException(String msg, int code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}
	
	public RRException(String msg, int code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}
	
}
