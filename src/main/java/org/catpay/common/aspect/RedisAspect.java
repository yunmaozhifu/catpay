package org.catpay.common.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.catpay.common.exception.RRException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

/**
 * Redis切面处理类
 *
 */
@Aspect
@Configuration
@Slf4j
public class RedisAspect {
    @Value("${spring.redis.open: false}")
    private boolean open;
	@Around("execution(* org.catpay.common.utils.RedisUtils.*(..))")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		if(open){
		try {
			return point.proceed();
		} catch (Exception e) {
			log.error("redis error", e);
			throw new RRException("Redis服务异常");
		}}
		return null;
	}
}
