package org.catpay.modules.app.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.R;
import org.catpay.modules.app.annotation.Login;
import org.catpay.modules.app.annotation.LoginUser;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.service.UserService;
import org.catpay.modules.app.service.impl.AlipayServiceImpl;
import org.catpay.modules.app.service.impl.QiniuyunServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * APP测试接口
 *
 */

@RestController
@RequestMapping("/app")
@Api("APP测试接口")
public class AppTestController {
	@Autowired
	private UserService userService;
	@Autowired
	private AlipayServiceImpl alipayService;
	@Autowired
	private QiniuyunServiceImpl qiniuyunService;

	@Login
	@GetMapping("userInfo")
	@ApiOperation("获取用户信息")
	public R<?> userInfo(@LoginUser UserEntity user) {
		return R.ok(CommonMap.getMap().put("user", user));
	}

	@Login
	@GetMapping("userId")
	@ApiOperation("获取用户ID")
	public R<?> userInfo(@RequestAttribute("userId") Integer userId) {
		return R.ok(CommonMap.getMap().put("userId", userId));
	}

	@GetMapping("notToken")
	@ApiOperation("忽略Token验证测试")
	public R<?> notToken() {
		return R.ok(CommonMap.getMap().put("msg", "无需token也能访问。。。"));
	}

	@GetMapping("sendsms")
	@ApiOperation("测试发送短信")
	public R<?> sendsms(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> params = (Map<String, String>) request.getAttribute("params");
		String phone = request.getParameter("phone");
		Assert.hasLength(phone, "手机号码不能为空");
		userService.sendSms(phone);
		return R.ok();
	}

	@GetMapping("aliPay")
	@ApiOperation("测试支付")
	public R<?> aliPay(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> params = (Map<String, String>) request.getAttribute("params");

		return R.ok(alipayService.createAlipayOrder("创鑫管家商品", "测试", "o_20190814101800767", "0.1",
				"http://61.140.20.194:8082/catpay/app/pay/apilypayNotify"));
	}

	@GetMapping("qr")
	@ApiOperation("测试支付")
	public R<?> qr(HttpServletRequest request, HttpServletResponse response) {
		String phone = request.getParameter("phone");
		// qiniuyunService.makeSharePage(phone, "你的好友"+phone+"推荐您加入", phone);
		return R.ok();
	}

}
