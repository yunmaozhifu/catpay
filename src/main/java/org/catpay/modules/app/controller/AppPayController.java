package org.catpay.modules.app.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.R;
import org.catpay.modules.app.annotation.Login;
import org.catpay.modules.app.annotation.LoginUser;
import org.catpay.modules.app.entity.OrderDataEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/app")
@Api(value = "订单controller", tags = { "订单操作接口" })
@Slf4j

public class AppPayController {
	@Autowired
	PayService payService;

	// 下单
	@Login
	@PostMapping("order/setOrderData")
	@ApiOperation("下单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "machinesId", value = "产品ID", required = true, dataType = "String", paramType = "query"),
			// @ApiImplicitParam(name = "status", value = "
			// 0全部1待付款2待发货3待收货4已完成", required = true, dataType =
			// "String",paramType = "query"),
			@ApiImplicitParam(name = "addressId", value = "地址ID", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "totalCount", value = "购买数量", required = true, dataType = "String", paramType = "query")

	})
	public R<?> setOrderData(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// String userId = request.getParameter("userId");
		String goodsId = request.getParameter("machinesId");
		String addressId = request.getParameter("addressId");
		String totalCount = request.getParameter("totalCount");

		// Assert.hasLength(userId, "userId不能为空");
		Assert.hasLength(goodsId, "machinesId不能为空");
		Assert.hasLength(addressId, "addressId不能为空");
		Assert.hasLength(totalCount, "totalCount不能为空");

		Long mid = Long.valueOf(goodsId);
		Integer tc = Integer.valueOf(totalCount);
		Long aid = Long.valueOf(addressId);

		String no = payService.setOrderData(user, mid, tc, aid);
		return R.ok(CommonMap.getMap().put("outTradeNo", no));
	}

	// 支付
	@Login
	@PostMapping("pay/alipayRequest")
	@ApiOperation("支付")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "outTradeNo", value = "订单编号", required = true, dataType = "String", paramType = "query")

	})
	public R<?> alipayRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String outTradeNo = request.getParameter("outTradeNo");
		Assert.hasLength(outTradeNo, "订单号不能为空");

		String result = payService.createAlipayOrder(outTradeNo);
		// R.ok(result);
		R<String> r = new R<String>();
		r.setResult(result);
		return r;
	}

	/**
	 * 支付宝支付异步通知
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@PostMapping("pay/apilypayNotify")
	@ApiOperation("支付通知")
	public void apilypayNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map requestParams = request.getParameterMap();
		boolean flag = payService.verifyNotify(requestParams);
		if (flag) {
			// 商户订单号
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
			log.info("商户订单号：" + out_trade_no);
			// 交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");
			log.info("交易状态：" + trade_status);

			// 支付成功处理逻辑
			if (trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")) {
				log.info("支付宝通知成功,后台处理结果为：SUCCESS");
				payService.payNotice(out_trade_no);

			}
		} else {
			// 验证失败
			log.info("参数验证失败");

		}
	}

	// 获取订单
	@Login
	@PostMapping("user/getOrderData")
	@ApiOperation("获取订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orderId", value = "orderId", required = false, dataType = "String", paramType = "query"),

	})
	public R<?> getOrderData(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {
		String orderId = request.getParameter("orderId");

		OrderDataEntity l = payService.getOrderData(Long.parseLong(orderId));
		return R.ok(CommonMap.getMap().put("orderData", l));
	}

	@Login
	@PostMapping("user/getOrderList")
	@ApiOperation("获取订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "userId", required = false, dataType = "String", paramType = "query"),
			// @ApiImplicitParam(name = "status", value = "
			// 0全部1待付款2待发货3待收货4已完成", required = true, dataType =
			// "String",paramType = "query"),
			@ApiImplicitParam(name = "pageNo", value = "当前页码", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "pageSzie", value = "一页多少", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getOrderList(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String status = "0";
		String pageNo = StrUtil.blankToDefault(request.getParameter("pageNo"), "0");
		String pageSzie = StrUtil.blankToDefault(request.getParameter("pageSize"), "20");

		List<OrderDataEntity> l = payService.getOrder(user.getId(), status, pageNo, pageSzie);
		return R.ok(CommonMap.getMap().put("orderList", l));
	}

	@Login
	@PostMapping("user/getNonPayOrderList")
	@ApiOperation("获取代付款订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "userId", required = false, dataType = "String", paramType = "query"),
			// @ApiImplicitParam(name = "status", value = "
			// 0全部1待付款2待发货3待收货4已完成", required = true, dataType =
			// "String",paramType = "query"),
			@ApiImplicitParam(name = "pageNo", value = "当前页码", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "pageSzie", value = "一页多少", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getNonPayOrderList(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String status = "1";
		String pageNo = StrUtil.blankToDefault(request.getParameter("pageNo"), "0");
		String pageSzie = StrUtil.blankToDefault(request.getParameter("pageSize"), "20");

		List<OrderDataEntity> l = payService.getOrder(user.getId(), status, pageNo, pageSzie);
		return R.ok(CommonMap.getMap().put("orderList", l));
	}

	@Login
	@PostMapping("user/getNonSendList")
	@ApiOperation("获取代发货订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "userId", required = false, dataType = "String", paramType = "query"),
			// @ApiImplicitParam(name = "status", value = "
			// 0全部1待付款2待发货3待收货4已完成", required = true, dataType =
			// "String",paramType = "query"),
			@ApiImplicitParam(name = "pageNo", value = "当前页码", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "pageSzie", value = "一页多少", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getNonSendList(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String status = "2";
		String pageNo = StrUtil.blankToDefault(request.getParameter("pageNo"), "0");
		String pageSzie = StrUtil.blankToDefault(request.getParameter("pageSize"), "20");

		List<OrderDataEntity> l = payService.getOrder(user.getId(), status, pageNo, pageSzie);
		return R.ok(CommonMap.getMap().put("orderList", l));
	}

	@Login
	@PostMapping("user/getSendList")
	@ApiOperation("获取待收货订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "userId", required = false, dataType = "String", paramType = "query"),
			// @ApiImplicitParam(name = "status", value = "
			// 0全部1待付款2待发货3待收货4已完成", required = true, dataType =
			// "String",paramType = "query"),
			@ApiImplicitParam(name = "pageNo", value = "当前页码", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "pageSzie", value = "一页多少", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getSendList(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String status = "3";
		String pageNo = StrUtil.blankToDefault(request.getParameter("pageNo"), "0");
		String pageSzie = StrUtil.blankToDefault(request.getParameter("pageSize"), "20");

		List<OrderDataEntity> l = payService.getOrder(user.getId(), status, pageNo, pageSzie);
		return R.ok(CommonMap.getMap().put("orderList", l));
	}

	@Login
	@PostMapping("user/getCompletedList")
	@ApiOperation("获取已完成订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "userId", required = false, dataType = "String", paramType = "query"),
			// @ApiImplicitParam(name = "status", value = "
			// 0全部1待付款2待发货3待收货4已完成", required = true, dataType =
			// "String",paramType = "query"),
			@ApiImplicitParam(name = "pageNo", value = "当前页码", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "pageSzie", value = "一页多少", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getCompletedList(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String status = "4";
		String pageNo = StrUtil.blankToDefault(request.getParameter("pageNo"), "0");
		String pageSzie = StrUtil.blankToDefault(request.getParameter("pageSize"), "20");

		List<OrderDataEntity> l = payService.getOrder(user.getId(), status, pageNo, pageSzie);
		return R.ok(CommonMap.getMap().put("orderList", l));
	}

	// 取消订单
	@Login
	@PostMapping("user/cancelOrder")
	@ApiOperation("取消订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orderId", value = "orderId", required = false, dataType = "String", paramType = "query"),

	})
	public R<?> cancelOrder(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {
		String outTradeNo = request.getParameter("orderId");

		payService.cancelOrder(user.getId(), outTradeNo);
		return R.ok();
	}
}
