package org.catpay.modules.app.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.R;
import org.catpay.modules.app.annotation.Login;
import org.catpay.modules.app.annotation.LoginUser;
import org.catpay.modules.app.contants.SysConfig;
import org.catpay.modules.app.entity.ProductEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.service.ManagerService;
import org.catpay.modules.app.service.UserService;
import org.catpay.modules.app.vo.PartnerInfo;
import org.catpay.modules.app.vo.SharePageInfo;
import org.catpay.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/app")
@Api(value = "APPcontroller", tags = { "app页面管理接口" })

public class AppManagerController {
	@Autowired
	ManagerService mService;
	@Autowired
	private UserService userService;
	@Autowired
	SysConfigService sysConfigService;
	@Value("${qiniu.cdn}")
	private String cdn;
	// 获取推荐产品

	@PostMapping("home/getRecommendGoods")
	@ApiOperation("获取首页推荐信息/会员虚拟产品")
	public R<?> getRecommendGoods(HttpServletRequest request, HttpServletResponse response) {
		List l = mService.getRecommendGoods();
		return R.ok(CommonMap.getMap().put("recommendGoodsList", l));
	}

	// 获取热销产品
	@PostMapping("home/getHotGoods")
	@ApiOperation("获取首页热门产品")
	public R<?> getHotGoods(HttpServletRequest request, HttpServletResponse response) {
		List l = mService.getHotGoods();
		return R.ok(CommonMap.getMap().put("hotGoodsList", l));
	}

	@PostMapping("home/getGoods")
	@ApiOperation("获取首页热门产品byID")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsId", value = "产品ID", required = true, dataType = "String", paramType = "query")

	})
	public R<?> getGoods(HttpServletRequest request, HttpServletResponse response) {
		String goodsId = request.getParameter("goodsId");
		Assert.hasLength(goodsId, "goodsId不能为空");
		ProductEntity l = mService.getGoods(Long.parseLong(goodsId));
		return R.ok(CommonMap.getMap().put("hotGoodsList", l));
	}

	// 获取公共消息(头条)
	@PostMapping("home/getHotMsg")
	@ApiOperation("获取首页热门信息")
	public R<?> getHotMsg(HttpServletRequest request, HttpServletResponse response) {

		List l = mService.getHotMsg();
		return R.ok(CommonMap.getMap().put("hotMsgList", l));
	}

	// 获取合伙人列表
	@Login
	@PostMapping("agent/getPartnerList")
	@ApiOperation("获取代理合伙人列表（头部固定为父级合伙人）")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "nickName", value = "合伙人名", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getPartnerList(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {
		String userName = request.getParameter("nickName");
		List l = userService.getUserPartner(user.getId(), userName);
		return R.ok(CommonMap.getMap().put("partnerList", l));
	}

	// 获取某个合伙人详情
	@PostMapping("agent/getPartner")
	@ApiOperation("获取伙伴详（包含全部的伙伴分级和，已经输入月份的分级和）")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "dateTime", value = "dateTime默认为本月，格式yyyy-MM", required = false, dataType = "String", paramType = "query")

	})
	public R<?> getPartner(HttpServletRequest request, HttpServletResponse response) {
		String userId = request.getParameter("userId");
		String dateTime = request.getParameter("dateTime");

		Assert.hasLength(userId, "userId不能为空");
		if (dateTime == null) {
			dateTime = DateUtil.format(new Date(), "yyyy-MM");
		}

		PartnerInfo l = mService.getPartner(Long.parseLong(userId), dateTime);
		PartnerInfo all = mService.getPartner(Long.parseLong(userId));

		return R.ok(CommonMap.getMap().put("partnerALL", all).put("pathnerMonth", l).put("userInfo",
				userService.selectById(Long.parseLong(userId))));

	}

	// 获取提现规则
	@PostMapping("home/getRule")
	@ApiOperation("获取提现规则（文本）")
	public R<?> getRule(HttpServletRequest request, HttpServletResponse response) {
		String l = sysConfigService.getValue(SysConfig.RULE);
		return R.ok(CommonMap.getMap().put("rule", l));
	}

	// 获取分享页
	@Login
	@PostMapping("home/getShareImage")
	@ApiOperation("获取分享页")
	public R<?> getSharePage(@LoginUser @ApiIgnore UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {

		List<SharePageInfo> l = new SharePageInfo().selectList(new EntityWrapper<SharePageInfo>().eq("user_id", -1));
		return R.ok(l);
	}

	// 获取关于我们
	@PostMapping("home/getAboutme")
	@ApiOperation("获取关于我们")
	public R<?> getAboutme(HttpServletRequest request, HttpServletResponse response) {
		String l = sysConfigService.getValue(SysConfig.ABOUTME);
		return R.ok(CommonMap.getMap().put("aboutme", l));
	}

	public static void main(String[] args) {
		System.out.print(DateUtil.format(new Date(), "yyyy-MM"));
	}
}
