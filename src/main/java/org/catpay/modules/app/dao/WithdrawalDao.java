package org.catpay.modules.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.app.entity.WithdrawalDataEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;
@Mapper
public interface WithdrawalDao extends BaseMapper<WithdrawalDataEntity>{

}
