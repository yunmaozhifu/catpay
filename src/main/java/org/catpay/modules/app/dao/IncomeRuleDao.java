package org.catpay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.app.entity.IncomeRuleEntity;
import org.catpay.modules.app.entity.IncomeRuleEntityDTO;

import java.util.List;
import java.util.Map;

@Mapper
public interface IncomeRuleDao extends BaseMapper<IncomeRuleEntity>{

    List<IncomeRuleEntityDTO> queryIncomeRuleEntity(Map<String, Object> params);

    int queryIncomeRuleEntityCount(Map<String, Object> params);

}
