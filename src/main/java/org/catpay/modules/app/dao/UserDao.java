package org.catpay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.catpay.modules.app.entity.UserAddressEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.vo.PartnerInfo;

import java.util.List;

/**
 * 用户
 * 
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

    void updatePassword(@Param("phone")String phone, @Param("password")String password);

    void setAddress(UserAddressEntity userAddressEntity);
    void setDefaultAddress(UserAddressEntity userAddressEntity);
    void updateAddress(UserAddressEntity userAddressEntity);
    void setAddressUnDe(@Param("userId")Long userId);
    List<UserAddressEntity> getAddress(@Param("userId")Long userId);
    UserAddressEntity getAddressById(@Param("addId")Long addId);
    PartnerInfo getPartner(@Param("userId")Long userId);
    List<Long> getSharePage();
}
