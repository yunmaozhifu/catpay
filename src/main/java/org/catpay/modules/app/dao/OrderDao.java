package org.catpay.modules.app.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.catpay.modules.app.entity.OrderDataEntity;
import org.catpay.modules.app.entity.OrderDataEntityDTO;

import com.baomidou.mybatisplus.mapper.BaseMapper;
@Mapper
public interface OrderDao extends BaseMapper<OrderDataEntity>{

	List<OrderDataEntityDTO> getOneOrder(@Param("orderId")String orderId);
}
