package org.catpay.modules.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.app.vo.SharePageInfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;

@Mapper
public interface SharePageDao  extends BaseMapper<SharePageInfo>{

}
