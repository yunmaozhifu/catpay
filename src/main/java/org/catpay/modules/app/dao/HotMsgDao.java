package org.catpay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.app.entity.HotMsgEntity;

@Mapper
public interface HotMsgDao extends BaseMapper<HotMsgEntity> {
}
