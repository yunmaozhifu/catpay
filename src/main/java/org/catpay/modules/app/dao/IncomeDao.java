package org.catpay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.catpay.modules.app.entity.IncomeDataEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.entity.WithdrawalDataEntity;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface IncomeDao extends BaseMapper<IncomeDataEntity>{

    /**
     * 查询收益明细
     * @param userId
     * @param start
     * @param end
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<IncomeDataEntity> getUserIncomeData(@Param("userId")long userId,
                                             @Param("start")String start,
                                             @Param("end")String end,
                                             @Param("pageNo")Integer pageNo,
                                             @Param("pageSize")Integer pageSize);

    /**
     * 获取时间段内收益和
     * @param userId
     * @param starttime
     * @param endtime
     * @return
     */
    BigDecimal getUserIncome(@Param("userId")long userId,
                             @Param("starttime")String starttime,
                             @Param("endtime")String endtime);

    /**
     * 查询审核中提现记录
     * @param userId
     * @return
     */
    int queryWithdrawCount(@Param("userId")long userId);

    /**
     * 记录提现记录
     * @param withdrawalDataEntity
     */
    void withdraw(WithdrawalDataEntity withdrawalDataEntity);

    /**
     * 更新冻结金额
     * @param userId
     * @param amount
     */
    void updateFrozenBalance(@Param("userId")long userId,@Param("amount")BigDecimal amount);

    /**
     * 分页查询提现记录
     * @param userId
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<WithdrawalDataEntity> getWithdrawalData(@Param("userId")long userId,
                                                 @Param("pageNo")Integer pageNo,
                                                 @Param("pageSize")Integer pageSize);

    /**
     * 根据id查询提现记录
     * @param id
     * @return
     */
    WithdrawalDataEntity queryWithdraw(@Param("id")long id);

    /**
     * 修改提现记录状态
     * @param id
     * @param status
     */
    void withdrawResult(@Param("id")long id,@Param("status")Integer status);

    /**
     * 更新余额
     * @param userId
     * @param amount
     */
    void updateAccountBalance(@Param("userId")long userId,@Param("amount")BigDecimal amount);

    /**
     * 查询用户信息
     * @param id
     * @return
     */
    UserEntity queryUser(@Param("id")long id);

}
