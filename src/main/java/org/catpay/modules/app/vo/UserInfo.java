package org.catpay.modules.app.vo;

import lombok.Data;

@Data
public class UserInfo {
	private String headImage;
	private Long id;
	private String nickName;
	private String phone;
	private String recommonNumber;// --用户推荐码
}
