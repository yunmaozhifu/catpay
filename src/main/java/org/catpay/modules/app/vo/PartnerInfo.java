package org.catpay.modules.app.vo;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PartnerInfo {
	// 下级合伙人数
	private Integer sum0Partner;
	private Integer sum1Partner;
	private Integer sum2Partner;
	private Integer sum3Partner;
	// 下级合伙人总共收益
	private BigDecimal sumAmount;
	// 下级合伙人 物品合
	private Integer sumGoods;
}
