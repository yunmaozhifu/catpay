package org.catpay.modules.app.vo;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@Data
@TableName("ape_share_image")
public class SharePageInfo extends Model<SharePageInfo> implements Serializable {

	private static final long serialVersionUID = 3877337541305639796L;

	@TableId
	private Long id;
	private String img;

	private Integer orderNo;

	private String comment;

	private Long userId;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public SharePageInfo(String img, Long userId) {
		this.img = img;
		this.userId = userId;
	}

	public SharePageInfo() {
	}
}
