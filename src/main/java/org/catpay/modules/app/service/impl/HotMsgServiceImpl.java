package org.catpay.modules.app.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.catpay.modules.app.dao.HotMsgDao;
import org.catpay.modules.app.entity.HotMsgEntity;
import org.catpay.modules.app.service.HotMsgService;
import org.springframework.stereotype.Service;

@Service("hotMsgService")
public class HotMsgServiceImpl extends ServiceImpl<HotMsgDao, HotMsgEntity> implements HotMsgService {
}
