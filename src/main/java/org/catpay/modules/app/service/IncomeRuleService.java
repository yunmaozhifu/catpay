package org.catpay.modules.app.service;

import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.IncomeRuleEntity;
import org.catpay.modules.app.entity.ProductEntity;

import com.baomidou.mybatisplus.service.IService;

/**
 * 奖励规则
 */
public interface IncomeRuleService extends IService<IncomeRuleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	List<ProductEntity> getRuleGoods();
}
