package org.catpay.modules.app.service;

import java.util.List;

import org.catpay.modules.app.entity.IncomeRuleEntity;
import org.catpay.modules.app.entity.OrderDataEntity;

public interface RewardService {
	/**
	 * 获取收益规则 状态不为-1的
	 * 
	 * @param goodsId
	 *            产品ID
	 * @return
	 */
	List<IncomeRuleEntity> getIncomeRule(Long goodsId);

	/**
	 * 根据规则 发放奖励
	 * 
	 * @param order
	 */
	public void doReward(OrderDataEntity order);
}
