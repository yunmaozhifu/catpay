package org.catpay.modules.app.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.IncomeDataEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.entity.WithdrawalDataEntity;

import com.baomidou.mybatisplus.service.IService;

public interface AccService extends IService<IncomeDataEntity> {
	/**
	 * 查询收益明细
	 * 
	 * @param userId
	 * @param start
	 * @param end
	 * @param pageNo
	 * @param pageSzie
	 * @return
	 */
	public List<IncomeDataEntity> getUserIncomeData(long userId, String start, String end, String pageNo,
			String pageSzie);

	/**
	 * 获取时间段内收益和
	 * 
	 * @param userId
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	public BigDecimal getUserIncome(long userId, String starttime, String endtime);

	/**
	 * 记录提现记录，注意更新userinfo 的冻结金额 提现金额小于 手续费 不可提现，提现金额大于 账户余额-冻结 不可以提现
	 * 
	 * @param user
	 * @param amount
	 * @param aliId
	 * @param name
	 * @param password
	 */
	public void withdraw(UserEntity user, String amount, String aliId, String name, String password);

	/**
	 * 获取提现记录
	 * 
	 * @param userId
	 * @param pageNo
	 * @param pageSzie
	 * @return
	 */
	public List<WithdrawalDataEntity> getWithdrawalData(long userId, String pageNo, String pageSzie);

	/**
	 * 修改提现记录状态
	 * 
	 * @param id
	 *            提现记录ID
	 * @param status
	 *            2提现成功3提现失败
	 */
	public void withdrawResult(Long id, Integer status);

	/**
	 * 添加收益
	 * 
	 * @param userId
	 * @param amount
	 * @param orderNo
	 * @param type
	 * @param buyUserId
	 * @param rule
	 * @param remark
	 */
	public void addUserIncome(long userId, BigDecimal amount, String orderNo, String type, Long buyUserId, String rule,
			String remark);

	public PageUtils queryUserIncomeDataPage(Map<String, Object> params);
}
