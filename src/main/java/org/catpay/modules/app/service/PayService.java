package org.catpay.modules.app.service;

import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.OrderDataEntity;
import org.catpay.modules.app.entity.UserEntity;

public interface PayService {
	/**
	 * 创建订单 ，扎入订单表 物流表 并返回 订单号
	 * 
	 * @param userId
	 * @param goodsId
	 * @param addId
	 * @return
	 */
	public String setOrderData(UserEntity user, Long goodsId, int tc, Long addId);

	/**
	 * 调用支付宝支付 alipayService
	 * 
	 * @param outTradeNo
	 */
	public String createAlipayOrder(String outTradeNo);

	/**
	 * 验证 回调参数可靠性 (已有代码) alipayService
	 * 
	 * @param para
	 * @return
	 */
	public boolean verifyNotify(Map para);

	/**
	 * 支付宝回调 回调扣费成功需对订单状态，发货单等修改 根据 奖励 规则 给与相关人员奖励 RewardServiceImpl.doReward
	 * 
	 * @param outTradeNo
	 */
	public void payNotice(String outTradeNo);

	/**
	 * 获取用户订单
	 * 
	 * @param userId
	 * @param status
	 *            订单状态。1：待付款；2：待发货；3：待收货；4：已完成；5：已失效；6：已取消；7：申请退款；8：已退款', 99 全部
	 * @return
	 */
	public List<OrderDataEntity> getOrder(Long userId, String status, String pageNo, String pageSzie);

	public OrderDataEntity getOrderData(Long userId);

	/**
	 * 取消订单 只有代付款的可以取消
	 * 
	 * @param userId
	 * @param outTradeNo
	 */
	public void cancelOrder(Long userId, String outTradeNo);

	public PageUtils getAllOrder(Map<String, Object> params);

	public void changeOrderStatus(String orderId, int status);

	public OrderDataEntity getOneOrder(String orderId);

	public boolean updateOrder(OrderDataEntity params);

	// public void changeOrderAmount(String orderId,BigDecimal money);
	//
	// public void changeOrderCount(String orderId,int num);
}
