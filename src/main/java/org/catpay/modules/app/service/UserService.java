package org.catpay.modules.app.service;

import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.UserAddressEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.vo.PartnerInfo;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.service.IService;

/**
 * 用户
 * 
 */
public interface UserService extends IService<UserEntity> {

	UserEntity queryByMobile(String mobile);

	/**
	 * 用户登录
	 * 
	 * @param form
	 *            登录表单
	 * @return 返回用户ID
	 */
	public UserEntity login(String phone, String password);

	/**
	 * 发送验证码
	 * 
	 * @param tel
	 */
	public void sendSms(String tel);

	/**
	 * 获取用户 伙伴 第一个为 父伙伴 其它为子伙伴
	 * 
	 * @return
	 */
	public List<UserEntity> getUserPartner(Long userId, String userName);

	/**
	 * 根据ID 获取用户信息
	 * 
	 * @param userId
	 * @return
	 */
	public UserEntity getUser(Long userId);

	/**
	 * 修改密码
	 * 
	 * @param phone
	 * @param password
	 */
	public void updatePassword(String phone, String password);

	/**
	 * 设置 地址
	 * 
	 * @param user
	 * @param name
	 * @param phone
	 * @param province
	 * @param city
	 * @param area
	 * @param address
	 * @param type
	 * @return
	 */
	public UserAddressEntity setAddress(UserEntity user, String name, String phone, String province, String city,
			String area, String address, String type);

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserAddressEntity> getAddress(Long userId);

	UserEntity updateUserInfo(long uid, String headImage, String nickName);

	String updateUserHeadImage(long uid, MultipartFile headImage);

	/** 分页获取所有商户信息 */
	public PageUtils queryPage(Map<String, Object> params);

	public PartnerInfo getPartner(Long userId, String dateTime);

	public PartnerInfo getPartner(Long userId);

	public void setDefaultAddress(UserEntity user, Long addid);

	public UserAddressEntity updateAddress(UserEntity user, Long addid, String name, String phone, String province,
			String city, String area, String address);

	List<Long> getSharePage();
}
