package org.catpay.modules.app.service;

import java.util.List;

import org.catpay.modules.app.entity.HotMsgEntity;
import org.catpay.modules.app.entity.ProductEntity;
import org.catpay.modules.app.vo.PartnerInfo;

public interface ManagerService {

	/**
	 * 返回推荐产品列表 这个list 为产品对象 在管理平台定义
	 * 
	 * @return
	 */
	public List<ProductEntity> getRecommendGoods();

	/**
	 * 返回热门产品 这个list 为产品对象 在管理平台定义
	 * 
	 * @return
	 */
	public List<ProductEntity> getHotGoods();

	/**
	 * 返回热门头条消息 查询消息表 按时间和 排序值排序
	 * 
	 * @return
	 */

	public List<HotMsgEntity> getHotMsg();

	public ProductEntity getGoods(Long id);

	/**
	 * 获取 该用户的 下级数量 ，所有下级卖出产品合目前默认100
	 * 
	 * @param userId
	 * @return
	 */
	public PartnerInfo getPartner(Long userId, String DateTime);

	public PartnerInfo getPartner(Long userId);
}
