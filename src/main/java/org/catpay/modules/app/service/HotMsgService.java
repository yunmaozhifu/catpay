package org.catpay.modules.app.service;

import com.baomidou.mybatisplus.service.IService;
import org.catpay.modules.app.entity.HotMsgEntity;

/**
 *  热门头条消息
 */
public interface HotMsgService extends IService<HotMsgEntity> {
}
