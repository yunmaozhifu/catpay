package org.catpay.modules.app.service.impl;

import java.util.List;

import org.catpay.modules.app.entity.HotMsgEntity;
import org.catpay.modules.app.entity.ProductEntity;
import org.catpay.modules.app.service.HotMsgService;
import org.catpay.modules.app.service.ManagerService;
import org.catpay.modules.app.service.UserService;
import org.catpay.modules.app.vo.PartnerInfo;
import org.catpay.modules.sys.service.BusinessProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;

@Service("managerService")
public class ManagerServiceImpl implements ManagerService {
	@Autowired
	BusinessProductService pService;
	@Autowired
	HotMsgService hotMsgService;
	@Autowired
	UserService userService;

	@Override
	public List<ProductEntity> getRecommendGoods() {
		return pService.getBannerProduct();
	}

	@Override
	public List<ProductEntity> getHotGoods() {

		return pService.getAllProduct();
	}

	@Override
	public List<HotMsgEntity> getHotMsg() {
		return hotMsgService
				.selectList(new EntityWrapper<HotMsgEntity>().orderBy("sort_num", false).orderBy("create_time", false));
	}

	@Override
	public PartnerInfo getPartner(Long userId, String DateTime) {
		return userService.getPartner(userId, DateTime);
	}

	@Override
	public PartnerInfo getPartner(Long userId) {
		return userService.getPartner(userId);
	}

	@Override
	public ProductEntity getGoods(Long id) {
		return pService.getProduct(id);
	}

}
