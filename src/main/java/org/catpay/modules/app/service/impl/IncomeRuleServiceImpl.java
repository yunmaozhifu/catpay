package org.catpay.modules.app.service.impl;

import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.dao.IncomeRuleDao;
import org.catpay.modules.app.entity.IncomeRuleEntity;
import org.catpay.modules.app.entity.IncomeRuleEntityDTO;
import org.catpay.modules.app.entity.ProductEntity;
import org.catpay.modules.app.service.IncomeRuleService;
import org.catpay.modules.sys.service.BusinessProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

@Service("incomeRuleService")
public class IncomeRuleServiceImpl extends ServiceImpl<IncomeRuleDao, IncomeRuleEntity> implements IncomeRuleService {

	@Autowired
	BusinessProductService pService;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		Integer limit = (Integer) params.get("limit");
		Integer page = ((Integer) params.get("page") - 1) * limit;
		params.put("page", page);
		List<IncomeRuleEntityDTO> IncomeRuleEntityList = baseMapper.queryIncomeRuleEntity(params);
		return new PageUtils(IncomeRuleEntityList, baseMapper.queryIncomeRuleEntityCount(params), limit, page);
	}

	@Override
	public List<ProductEntity> getRuleGoods() {
		return pService.getBannerProduct();
	}

}
