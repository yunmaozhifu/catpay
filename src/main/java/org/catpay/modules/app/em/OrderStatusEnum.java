package org.catpay.modules.app.em;

public enum OrderStatusEnum {
	////'订单状态。1：待付款；2：待发货；3：待收货；4：已完成；5：已失效；6：已取消；7：申请退款；8：已退款',
待付款(1),
待发货(2),
待收货(3),
已完成(4),
已失效(5),
已取消(6),
申请退款(7),
已退款(8);
private int value ;

public int getValue() {
	return value;
}

public void setValue(int value) {
	this.value = value;
}

private OrderStatusEnum(int value) {
	this.value = value;
}
	
}
