package org.catpay.modules.app.em;

public enum TypeGetIncomeEnum {
	 // day 返回最近7天的明细   month 返回最近1个月  明细  times 时间段
	day("day"),
	month("month"),
	times("times");
   private String type;

private TypeGetIncomeEnum(String type) {
	this.type = type;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

	

public static TypeGetIncomeEnum getTypeGetIncomeEnum(String type){
		for (TypeGetIncomeEnum t :TypeGetIncomeEnum.values()){
			if(t.getType().equals(type)) return t;
		}
		return day;
	}
}
