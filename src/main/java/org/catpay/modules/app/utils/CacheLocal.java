package org.catpay.modules.app.utils;
import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
public class CacheLocal {
 private static TimedCache<Object, Object> phone_code_cache = CacheUtil.newTimedCache(5*60*1000);
 static {
	 phone_code_cache.schedulePrune(10000);
 }
 public static String getCode(String phone){
	 return (String)phone_code_cache.get(phone);
 }

 public static void setPhoneCode(String phone,String code){
	 phone_code_cache.put(phone,code);
 }
 public static void removePhoneCode(String phone){
	 phone_code_cache.remove(phone);
 }
}
