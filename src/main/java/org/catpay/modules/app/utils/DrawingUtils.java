package org.catpay.modules.app.utils;


import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
 
import javax.imageio.ImageIO;
 


 
/**
 * Java 图片生成
 * 
 * @author wgy

 */
public class DrawingUtils {

	private static float jPEGcompression = 0.75f;// 图片清晰比率
 
	/**
	 * @Description : 将二维码图片和文字生成到一张图片上
	 * @Param : originalImg 原图
	 * @Param : qrCodeImg 二维码地址
	 * @Param : shareDesc 图片文字
	 * @return : java.lang.String
	 * @Author : houzhenghai
	 * @Date : 2018/8/15
	 */
	public static String generateImg(String originalImg, BufferedImage qrCodeImg, String shareDesc,String Lo) throws Exception {
		// 加载原图图片
		BufferedImage imageLocal = ImageIO.read(new URL(originalImg));
		// 加载用户的二维码
		BufferedImage imageCode = qrCodeImg;
		// 以原图片为模板
		Graphics2D g = imageLocal.createGraphics();
		AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		g.setComposite(ac);
		g.setBackground(Color.WHITE);
		// 在模板上添加用户二维码(地址,左边距,上边距,图片宽度,图片高度,未知)
		g.drawImage(imageCode, 250, imageLocal.getHeight() - 190, 160, 158, null);
		// 设置文本样式
		g.setFont(new Font("微软雅黑", Font.PLAIN, 40));
		g.setColor(Color.red);
		// 计算文字长度，计算居中的x点坐标
		//g.drawString(shareDesc, imageLocal.getWidth() - 190, imageLocal.getHeight() - 530);
		g.drawString(shareDesc, 5, imageLocal.getHeight() - 530);
		// 设置文本样式
		g.setFont(new Font("微软雅黑", Font.PLAIN + Font.BOLD, 16));
		g.setColor(Color.WHITE);
		// 计算文字长度，计算居中的x点坐标
		String caewm = "长按二维码";
		g.drawString(caewm, 290, imageLocal.getHeight() - 10);
		g.dispose();
		
		 ImageIO.write(imageLocal, "jpg", new File(Lo));
		return null;
	}
 

 
	
 
	

}
