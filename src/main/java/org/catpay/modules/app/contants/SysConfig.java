package org.catpay.modules.app.contants;

public class SysConfig {
	  public static final String RULE = "SYS_WITHDRAW_RULE";
	  public static final String SHAREPAGE = "SYS_SHAREPAGE";
	  public static final String ABOUTME = "SYS_ABOUTME";
	  public static final String ALINOTIFYURL = "PAY_ALINOTIFYURL";
	  public static final String CHARGE = "USER_CHARGE";
	  public static final String SHAREUSERURL = "USER_SHAREUSERURL";
}
