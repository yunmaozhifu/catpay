package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_hot_msg")
@Data
public class HotMsgEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4529630255299777881L;

	@TableId
	private Long id;
	
	private Long userId;
	
	private Integer type;
	private String title;
	private String context;
	private String link;
	private Integer isRead;
	private Integer sortNum;
	
	private Date createTime;
}
