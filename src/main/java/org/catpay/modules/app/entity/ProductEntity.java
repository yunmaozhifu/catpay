package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.catpay.common.annotation.Ignore;
import org.catpay.common.validator.group.AddGroup;
import org.catpay.common.validator.group.GroundingGroup;
import org.catpay.common.validator.group.UnableGroup;
import org.catpay.common.validator.group.UpdateGroup;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_product_info")
@Data
public class ProductEntity extends Model<ProductEntity> implements Serializable {

	private static final long serialVersionUID = 1L;

	@TableId
	@NotNull(message = "Id不能为空", groups = { UnableGroup.class, GroundingGroup.class, UpdateGroup.class })
	private Long id;

	@NotBlank(message = "标题不能为空", groups = AddGroup.class)
	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	private String title;// 标题

	@NotBlank(message = "副标题不能为空", groups = AddGroup.class)
	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	private String subtitle;// 副标题

	@NotBlank(message = "商品详情不能为空", groups = AddGroup.class)
	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	private String details;// 副标题

	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	@NotNull(message = "价格不能为空", groups = AddGroup.class)
	private BigDecimal price;// 价格

	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	@NotNull(message = "序号不能为空", groups = AddGroup.class)
	@TableField("pro_order")
	private Integer order;// 排序

	@NotNull(message = "类型不能为空", groups = AddGroup.class)
	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	private Integer type;// 类型

	@Ignore(groups = { UnableGroup.class, GroundingGroup.class })
	@TableField("imgUrl")
	private String imgUrl;// 商品图片

	@Ignore(groups = { UnableGroup.class, GroundingGroup.class, UpdateGroup.class })
	@TableField("createTime")
	private Date createTime;

	@TableField("isBannerProduct")
	@Ignore(groups = { GroundingGroup.class, UpdateGroup.class })
	private Boolean bannerProduct;

	@Ignore(groups = { UnableGroup.class, UpdateGroup.class })
	@TableField("isGrounding")
	private Boolean grounding;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
