package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_send_records")
@Data
public class GoodsSendEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@TableId
	private Long id;
	private String orderNo;
	private String sendNo;
	private Integer count;
	private Integer state;
	private Date createTime;
	private Date sendTime;
	private String expressNo;
	private String expressCompany;
	private String name;
	private String phone;
	private String province;
	private String city;
	private String area;
	private String address;

}
