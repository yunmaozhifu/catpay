package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@TableName("ape_user_info")
@Data
public class UserEntity extends Model<UserEntity> implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;
	private String nickName;
	private String phone;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;
	private Long fatherId;
	private String recommonNumber;
	private String fatherRecommonNumber;
	private Integer role;
	private Integer Level;
	private String headImage;
	private Date createTime;
	// 可提现金额
	private BigDecimal accountBalance;
	// 总收益
	private BigDecimal profitBalance;
	// 冻结金额
	private BigDecimal frozenBalance;
	private Integer status;

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
