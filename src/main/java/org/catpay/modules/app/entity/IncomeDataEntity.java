package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_income_records")
@Data
public class IncomeDataEntity implements Serializable {

	private static final long serialVersionUID = 6811125967044934347L;
	@TableId
	private Long id;

	// 拿笔订单使其 收益
	private String orderNo;
	// 收益规则
	private String rule;
	// 收益类型
	private String type;

	private BigDecimal amount;

	private Long userId;
	// 产生 此收益的 交易人
	private Long buyUserId;

	private String remark;

	private String phone;

	private String nickName;
	private Date createTime = new Date();
}
