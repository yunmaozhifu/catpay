package org.catpay.modules.app.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_user_address")
@Data
public class UserAddressEntity {

	private Long id;

	private Long userId;

	private String name;

	private String phone;

	private String province;

	private String city;

	private String area;

	private String address;

	private String isDefault;

	private String label;

	private Date createTime;

}
