package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_withdrawal_records")
@Data
public class WithdrawalDataEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;

	private Long userId;
	// 提现金额
	private BigDecimal amount;
	// 手续费
	private BigDecimal servicechargeAmount;

	private Date createTime;
	// 支付宝账号
	private String transferId;
	// 支付宝姓名
	private String toName;

	// 1审核中2提现成功3提现失败
	private Integer status;

	// 手机号
	private String phone;

	// 昵称
	private String nickName;

	// 备注
	private String remark;
}
