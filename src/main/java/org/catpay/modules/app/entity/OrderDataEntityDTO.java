package org.catpay.modules.app.entity;

import lombok.Data;

@Data
public class OrderDataEntityDTO extends OrderDataEntity {
	private String province;
	private String city;
	private String area;
	private String address;
}
