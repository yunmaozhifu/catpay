package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_income_rule")
@Data
public class IncomeRuleEntity implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	@TableId
	private Long id;
	// 产品
	private Long goodsId;
	// 数量 购买产品数量大于等于此 才给奖励
	private Integer cond;
	// 奖励给谁 1本人2推荐人3推荐人的推荐人4
	private Integer userChose;
	// 获得奖励的人需满足的条件
	private Integer toPatherRole;
	private Integer toPatherLevel;
	// 奖励金额 ，如果带% 则表示此订单总金额的百分比 否则为固定金额
	private String aumt;
	private Integer status;// -1停用
	private Date createTime;
}
