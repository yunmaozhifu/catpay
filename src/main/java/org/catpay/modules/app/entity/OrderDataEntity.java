package org.catpay.modules.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.catpay.common.validator.group.UpdateGroup;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

@TableName("ape_order_records")
@Data
public class OrderDataEntity implements Serializable {
	/**
	 * 	
	 */
	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;

	private Long userId;
	private String phone;
	private String nickName;
	// 对应支付宝交易号
	private String transferNo;
	@NotNull(message = "订单ID不能为空", groups = { UpdateGroup.class })
	private String orderNo;
	@NotNull(message = "订单金额不能为空", groups = { UpdateGroup.class })
	private BigDecimal amount;

	private Long goodsId;
	private Long addressId;
	private String goodsName;
	private String image;
	private BigDecimal goodsPrice;
	@NotNull(message = "数量不能为空", groups = { UpdateGroup.class })
	private Integer totalCount;
	// 1虚拟物品 2实物
	private Integer goodsType;
	// '订单状态。1：待付款；2：待发货；3：待收货；4：已完成；5：已失效；6：已取消；7：申请退款；8：已退款',
	@NotNull(message = "订单状态不能为空", groups = { UpdateGroup.class })
	private Integer status;

	private Date createTime;
}
