

package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysMenuEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 菜单管理
 * 
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {
	
}
