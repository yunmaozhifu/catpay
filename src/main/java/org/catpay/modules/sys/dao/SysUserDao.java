package org.catpay.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysUserEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 系统用户
 * 
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);
	

}
