package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysRoleEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 角色管理
 * 
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {

}
