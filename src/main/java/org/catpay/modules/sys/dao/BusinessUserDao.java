package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.app.entity.UserEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

@Mapper
public interface BusinessUserDao extends BaseMapper<UserEntity>{

}
