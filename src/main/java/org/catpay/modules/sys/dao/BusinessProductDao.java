package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.app.entity.ProductEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

@Mapper
public interface BusinessProductDao extends BaseMapper<ProductEntity>{

}
