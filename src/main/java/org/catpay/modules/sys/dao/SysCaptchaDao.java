package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysCaptchaEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 验证码
 *
 */
@Mapper
public interface SysCaptchaDao extends BaseMapper<SysCaptchaEntity> {

}
