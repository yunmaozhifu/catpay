package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysUserRoleEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 用户与角色对应关系
 * 
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {

}
