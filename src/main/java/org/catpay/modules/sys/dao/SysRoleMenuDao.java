package org.catpay.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysRoleMenuEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 角色与菜单对应关系
 * 
 */
@Mapper
public interface SysRoleMenuDao extends BaseMapper<SysRoleMenuEntity> {
	
}
