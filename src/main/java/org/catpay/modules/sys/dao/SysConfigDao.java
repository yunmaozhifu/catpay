
package org.catpay.modules.sys.dao;


import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysConfigEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 系统配置信息
 */
@Mapper
public interface SysConfigDao extends BaseMapper<SysConfigEntity> {
	
}
