
package org.catpay.modules.sys.dao;


import org.apache.ibatis.annotations.Mapper;
import org.catpay.modules.sys.entity.SysLogEntity;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 系统日志
 * 
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
