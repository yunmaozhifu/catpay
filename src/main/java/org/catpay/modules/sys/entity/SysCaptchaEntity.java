package org.catpay.modules.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.Data;

/**
 * 系统验证码
 */
@TableName("sys_captcha")
@Data
public class SysCaptchaEntity extends Model<SysCaptchaEntity> implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 2164244482933154084L;
	
	@TableId(type = IdType.INPUT)
    private String uuid;
    /**
     * 验证码
     */
    private String code;
    /**
     * 过期时间
     */
    private Date expireTime;
    
	@Override
	protected Serializable pkVal() {
		return this.uuid;
	}

   
}
