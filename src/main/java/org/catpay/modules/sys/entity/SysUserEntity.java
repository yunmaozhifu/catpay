package org.catpay.modules.sys.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.catpay.common.annotation.Ignore;
import org.catpay.common.validator.group.AddGroup;
import org.catpay.common.validator.group.UnableGroup;
import org.catpay.common.validator.group.UpdateGroup;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 系统用户
 * 
 */
@TableName("sys_user")
@Data
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户ID
	 */
	@TableId
	@NotNull(message="用户ID不能为空", groups = {UnableGroup.class, UpdateGroup.class})
	private Long userId;

	/**
	 * 用户名
	 */
	@NotBlank(message="用户名不能为空", groups = AddGroup.class)
	@Ignore(groups={UpdateGroup.class,UnableGroup.class})
	private String username;

	/**
	 * 密码
	 */
	@NotBlank(message="密码不能为空", groups = AddGroup.class)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)//Jackson
	@JSONField(serialize = false)//fastjson
	@Ignore(groups={UpdateGroup.class,UnableGroup.class})
	private String password;

	/**
	 * 盐
	 */
	@JsonIgnore
	private String salt;

	/**
	 * 邮箱
	 */
	@NotBlank(message="邮箱不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Email(message="邮箱格式不正确", groups = {AddGroup.class, UpdateGroup.class})
	@Ignore(groups={UnableGroup.class})
	private String email;

	/**
	 * 手机号
	 */
	@NotBlank(message="手机号不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Ignore(groups={UnableGroup.class})
	private String mobile;

	/**
	 * 状态  0：禁用   1：正常
	 */
	@NotNull(message="状态不能为空", groups = UnableGroup.class)
	private Integer status;
	
	
	
	/**
	 * 角色ID列表
	 */
	@TableField(exist=false)
	private List<Long> roleIdList;
	
	
	/**
	 * 创建者ID
	 */
	@Ignore(groups={UpdateGroup.class,UnableGroup.class})
	private Long createUserId;

	/**
	 * 创建时间
	 */
	@Ignore(groups={UpdateGroup.class,UnableGroup.class})
	private Date createTime;


	
}
