package org.catpay.modules.sys.entity;


import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

/**
 * 用户与角色对应关系
 */
@TableName("sys_user_role")
@Data
public class SysUserRoleEntity extends Model<SysUserRoleEntity> implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId
	private Long id;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 角色ID
	 */
	private Long roleId;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
