package org.catpay.modules.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.catpay.common.annotation.Ignore;
import org.catpay.common.validator.group.AddGroup;
import org.catpay.common.validator.group.UpdateGroup;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

/**
 * 角色
 * 
 */
@TableName("sys_role")
@Data
public class SysRoleEntity extends Model<SysRoleEntity> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 角色ID
	 */
	@TableId
	@NotNull(message="角色ID不能为空",groups=UpdateGroup.class)
	private Long roleId;

	/**
	 * 角色名称
	 */
	@NotBlank(message="角色名称不能为空",groups=AddGroup.class)
	@Ignore
	private String roleName;

	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 创建者ID
	 */
	private Long createUserId;

	@TableField(exist=false)
	private List<Long> menuIdList;
	
	/**
	 * 创建时间
	 */
	private Date createTime;

	@Override
	protected Serializable pkVal() {
		return this.roleId;
	}

	
	
}
