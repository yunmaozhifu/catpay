
package org.catpay.modules.sys.entity;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

/**
 * 系统配置信息
 * 
 */
@TableName("sys_config")
@Data
public class SysConfigEntity {
	
	@TableId
	private Long id;
	
	@NotBlank(message="参数名不能为空")
	private String paramKey;
	
	@NotBlank(message="参数值不能为空")
	private String paramValue;
	
	private String remark;
	
}
