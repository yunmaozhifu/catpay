package org.catpay.modules.sys.entity;


import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色与菜单对应关系
 * 
 */
@TableName("sys_role_menu")
@Data
public class SysRoleMenuEntity extends Model<SysRoleMenuEntity> implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;

	/**
	 * 角色ID
	 */
	private Long roleId;

	/**
	 * 菜单ID
	 */
	private Long menuId;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	
	
}
