
package org.catpay.modules.sys.redis;


import org.catpay.common.utils.Constant;
import org.catpay.common.utils.RedisUtils;
import org.catpay.modules.sys.entity.SysConfigEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 系统配置Redis
 *
 */
@Component
public class SysConfigRedis {
	
    @Autowired
    private RedisUtils redisUtils;
    
    public void saveOrUpdate(SysConfigEntity config) {
        if(config == null){
            return ;
        }
        String key = Constant.CONFIG_KEY + config.getParamKey();
        redisUtils.set(key, config);
    }

    public void delete(String configKey) {
        String key = Constant.CONFIG_KEY + configKey;
        redisUtils.delete(key);
    }

    public SysConfigEntity get(String configKey){
        String key = Constant.CONFIG_KEY + configKey;
        return redisUtils.get(key, SysConfigEntity.class);
    }
}
