package org.catpay.modules.sys.utils;

import org.apache.http.HttpStatus;

import lombok.Data;

/**
 * 返回数据
 * @param <T>
 * 
 */
@Data
public class R<T> {
	
	private Integer code;
	private String msg;
    private Boolean status;
    private T result;
	
	public R() {
		this.setCode(200);
		this.setStatus(true);
		this.setMsg("success");
	}
	
	public static R<?> error() {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "系统异常，请联系管理员");
	}
	
	public static R<?> error(String msg) {
		return error(HttpStatus.SC_NOT_ACCEPTABLE, msg);
	}
	
	public static R<?> error(int code, String msg) {
		R<?> r = new R<Object>();
		r.setCode(code);
		r.setMsg(msg);
		r.setStatus(false);
		return r;
	}

	public static R<?> ok(String msg) {
		R<?> r = new R<Object>();
		r.setMsg(msg);
		return r;
	}
	
	public static <T> R<T> ok(T result) {
		R<T> r = new R<T>();
		r.setResult(result);
		return r;
	}
	
	public static R<?> ok() {
		return new R<Object>();
	}

}
