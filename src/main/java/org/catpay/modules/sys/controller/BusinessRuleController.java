package org.catpay.modules.sys.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.IncomeRuleEntity;
import org.catpay.modules.app.service.IncomeRuleService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/business")
public class BusinessRuleController extends AbstractController {

	@Autowired
	private IncomeRuleService incomeRuleService;

	@PostMapping("rule/list")

	public R<?> ruleList(@RequestBody Map<String, Object> params) {
		PageUtils page = incomeRuleService.queryPage(params);
		return R.ok(CommonMap.getMap().put("page", page));
	}

	@PostMapping("rule/getRuleGoods")
	public R<?> getRuleGoods(HttpServletRequest request, HttpServletResponse response) {
		List l = incomeRuleService.getRuleGoods();
		return R.ok(CommonMap.getMap().put("ruleGoodsList", l));
	}

	@PostMapping("/rule/save")

	public R<?> save(@RequestBody @Validated IncomeRuleEntity incomeRuleEntity) {
		incomeRuleEntity.setCreateTime(new Date());
		incomeRuleService.insert(incomeRuleEntity);
		return R.ok();
	}

	@GetMapping("/rule/info/{id}")
	public R<?> info(@PathVariable("id") Long id) {
		IncomeRuleEntity incomeRuleEntity = incomeRuleService.selectById(id);
		return R.ok(CommonMap.getMap().put("incomeRule", incomeRuleEntity));
	}

	@PostMapping("/rule/update")

	public R<?> update(@RequestBody @Validated IncomeRuleEntity incomeRuleEntity) {
		incomeRuleService.updateById(incomeRuleEntity);
		return R.ok();
	}

	@PostMapping("/rule/disable")

	public R<?> enable(@RequestBody @Validated IncomeRuleEntity incomeRuleEntity) {
		incomeRuleEntity.setStatus(-1);
		incomeRuleService.updateById(incomeRuleEntity);
		return R.ok();
	}

	@PostMapping("/rule/enable")

	public R<?> disable(@RequestBody @Validated IncomeRuleEntity incomeRuleEntity) {
		incomeRuleEntity.setStatus(0);
		incomeRuleService.updateById(incomeRuleEntity);
		return R.ok();
	}

}
