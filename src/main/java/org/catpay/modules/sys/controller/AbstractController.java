package org.catpay.modules.sys.controller;

import org.apache.shiro.SecurityUtils;
import org.catpay.common.utils.Constant;
import org.catpay.modules.sys.entity.SysUserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 * 
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}
	
	protected boolean isSupperAdmin() {
		return getUserId()==Constant.SUPER_ADMIN;
	}
	
	protected  boolean isAdminGroup() {
		return getUser().getRoleIdList().contains(Constant.ADMIN_GROUP);
	}
}
