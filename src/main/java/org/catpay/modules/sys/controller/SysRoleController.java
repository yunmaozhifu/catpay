package org.catpay.modules.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.catpay.common.annotation.SysLog;
import org.catpay.common.annotation.UpdateIgnore;
import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.Constant;
import org.catpay.common.utils.PageUtils;
import org.catpay.common.validator.group.AddGroup;
import org.catpay.common.validator.group.UpdateGroup;
import org.catpay.modules.sys.entity.SysRoleEntity;
import org.catpay.modules.sys.service.SysRoleMenuService;
import org.catpay.modules.sys.service.SysRoleService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;


/**
 * 角色管理
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	/**
	 * 角色列表
	 */
	@PostMapping("/list")
	@RequiresPermissions("sys:role:list")
	public R<?> list(@RequestBody Map<String, Object> params){

		PageUtils page = sysRoleService.queryPage(params);

		return R.ok(CommonMap.getMap().put("page", page));
	}
	
	/**
	 * 角色列表
	 */
	@PostMapping("/select")
	@RequiresPermissions("sys:role:select")
	public R<?> select(){
		Map<String, Object> map = new HashMap<>();
		if(!isSupperAdmin()) {
			map.put("adminGroup", true);
		}
		
		List<SysRoleEntity> list = sysRoleService.selectList(
				new EntityWrapper<SysRoleEntity>()
				.ne(MapUtils.getBooleanValue(map, "adminGroup"), "role_id", 1));
		
		return R.ok(CommonMap.getMap().put("list", list));
	}
	
	/**
	 * 角色信息
	 */
	@GetMapping("/info/{roleId}")
	@RequiresPermissions("sys:role:info")
	public R<?> info(@PathVariable("roleId") Long roleId){
		SysRoleEntity role = sysRoleService.selectById(roleId);
		
		//查询角色对应的菜单
		List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);
		
		return R.ok(CommonMap.getMap().put("role", role));
	}
	
	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@PostMapping("/save")
	@RequiresPermissions("sys:role:save")
	public R<?> save(@RequestBody @Validated({AddGroup.class}) SysRoleEntity role){
		if(role.getMenuIdList().containsAll(Arrays.asList(Constant.IGNORE_MENU)))
			return R.error("非法操作");
		role.setCreateUserId(getUserId());
		sysRoleService.save(role);
		
		return R.ok();
	}
	
	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@PostMapping("/update")
	@RequiresPermissions("sys:role:update")
	@UpdateIgnore
	public R<?> update(@RequestBody @Validated({UpdateGroup.class}) @UpdateIgnore SysRoleEntity role){
		if(role.getMenuIdList().containsAll(Arrays.asList(Constant.IGNORE_MENU)))
			return R.error("非法操作");
		role.setCreateUserId(getUserId());
		sysRoleService.update(role);
		
		return R.ok();
	}
	
	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@PostMapping("/delete")
	@RequiresPermissions("sys:role:delete")
	public R<?> delete(@RequestBody Long[] roleIds){
		
		if(ArrayUtils.contains(roleIds, 1L)){
			return R.error("管理员角色不能删除");
		}
		sysRoleService.deleteBatch(roleIds);
		
		return R.ok();
	}
}
