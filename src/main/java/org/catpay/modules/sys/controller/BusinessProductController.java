package org.catpay.modules.sys.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.catpay.common.annotation.UpdateIgnore;
import org.catpay.common.exception.RRException;
import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.PageUtils;
import org.catpay.common.utils.Query;
import org.catpay.common.validator.group.AddGroup;
import org.catpay.common.validator.group.GroundingGroup;
import org.catpay.common.validator.group.UnableGroup;
import org.catpay.common.validator.group.UpdateGroup;
import org.catpay.modules.app.contants.SysConfig;
import org.catpay.modules.app.entity.ProductEntity;
import org.catpay.modules.app.service.impl.QiniuyunServiceImpl;
import org.catpay.modules.app.vo.SharePageInfo;
import org.catpay.modules.sys.service.BusinessProductService;
import org.catpay.modules.sys.service.SysConfigService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;


@RestController
@RequestMapping("/business")
public class BusinessProductController extends AbstractController{
	
	@Autowired
	private BusinessProductService businessProductService;
	
	@Autowired
	private QiniuyunServiceImpl qiniuyunService;
	
	@Autowired
	SysConfigService sysConfigService;
	
	@PostMapping("/productList")
	public R<?> productList(@RequestBody Map<String, Object> params){
		PageUtils page = businessProductService.queryPage(params);
		return R.ok(CommonMap.getMap().put("page", page));
	}
	
	@PostMapping("/productSave")
	public R<?> productSave(@RequestBody @Validated({AddGroup.class}) ProductEntity productEntity){
		productEntity.setCreateTime(new Date());
		businessProductService.productSave(productEntity);
		return R.ok();
	}
	
	@PostMapping("/setBanner")
	@UpdateIgnore(groups={UnableGroup.class})
	public R<?> unable(@RequestBody @Validated({UnableGroup.class}) @UpdateIgnore(groups={UnableGroup.class}) ProductEntity productEntity){
		businessProductService.updateById(productEntity);
		return R.ok();
	}
	
	
	@PostMapping("/setGrounding")
	@UpdateIgnore(groups={GroundingGroup.class})
	public R<?> setGrounding(@RequestBody @Validated({GroundingGroup.class}) @UpdateIgnore(groups={GroundingGroup.class}) ProductEntity productEntity){
		businessProductService.updateById(productEntity);
		return R.ok();
	}

	@UpdateIgnore(groups={UpdateGroup.class})
	@PostMapping("/productUpdate")
	public R<?> updatePro(@RequestBody @Validated({UpdateGroup.class}) @UpdateIgnore(groups={UpdateGroup.class}) ProductEntity productEntity){
		businessProductService.updateById(productEntity);
		return R.ok();
	}
	
	@GetMapping("/getProductInfo/{proId}")
	public R<?> getOnePro(@PathVariable("proId") String proId){
		return R.ok(new ProductEntity().selectById(proId));
	}
	
	@PostMapping("/upload")
	public R<?> upload(@RequestParam("file") MultipartFile file){
		return R.ok(CommonMap.getMap().put("url", businessProductService.uploadFile(file,"pro")));
	}
	
	@PostMapping("uploadDetail")
	public CommonMap uploadDetail(@RequestParam("file") MultipartFile file) {
		String url =  businessProductService.uploadFile(file,"detail");
		return CommonMap.getMap().put("code", 0)
		.put("msg", "成功").put("data", CommonMap.getMap().put("src", url));
	}
	
	@PostMapping("/sharePage")
	public R<?> sharePage(@RequestParam("file") MultipartFile file,HttpServletRequest request){
		String userId = request.getParameter("userId");
		String phone = request.getParameter("phone");
		String img = businessProductService.uploadFile(file,"share");
		String url = sysConfigService.getValue(SysConfig.SHAREUSERURL);
	    if(url ==null){
	    	url = "";
	    }
	    String content = url+phone;
		return R.ok(CommonMap.getMap().put("url",qiniuyunService.makeSharePage(content,"",UUID.fastUUID().toString(),img,Long.parseLong(userId))));
	}
	
	@PostMapping("/sharePageList")
	public R<?> sharePageList(){
		List<SharePageInfo> l = new SharePageInfo().selectList(new EntityWrapper<SharePageInfo>().eq("user_id", -1));
		PageUtils pu = new PageUtils(l,l.size(),1,1);
		return R.ok(CommonMap.getMap().put("page", pu));
	}
	
	@PostMapping("/editSharePage")
	public R<?> sharePageList(@RequestParam("file") MultipartFile file,HttpServletRequest request){
		String id =  request.getParameter("shareId");
		if(StrUtil.isBlank(id)) throw new RRException("id不能为空");
		String url = businessProductService.uploadFile(file,"share");
		SharePageInfo spi = new SharePageInfo();
		spi.setId(Long.valueOf(id));
		spi.setImg(url);
		spi.updateById();
		return R.ok(CommonMap.getMap().put("url",url));
	}
}
