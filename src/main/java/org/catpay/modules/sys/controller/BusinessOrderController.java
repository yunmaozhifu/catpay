package org.catpay.modules.sys.controller;

import java.util.Map;

import org.catpay.common.utils.CommonMap;
import org.catpay.common.validator.group.UpdateGroup;
import org.catpay.modules.app.entity.OrderDataEntity;
import org.catpay.modules.app.service.PayService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.map.MapUtil;

@RestController
@RequestMapping("/business")
public class BusinessOrderController extends AbstractController {

	@Autowired
	private PayService payService;


	@PostMapping("/orderList")
	public R<?> productList(@RequestBody Map<String, Object> params) {
		return R.ok(CommonMap.getMap().put("page", payService.getAllOrder(params)));
	}

	@PostMapping("/changeOrderStatus")
	public R<?> changeOrderStatus(@RequestBody Map<String, Object> params) {
		payService.changeOrderStatus(MapUtil.getStr(params, "orderId"), MapUtil.getInt(params, "status"));
		return R.ok();
	}

	@GetMapping("/getOrderInfo/{orderId}")
	public R<?> getOrderInfo(@PathVariable("orderId") String orderId) {
		return R.ok(payService.getOneOrder(orderId));
	}

	@PostMapping("/update")
	public R<?> updateOrder(@RequestBody @Validated({ UpdateGroup.class }) OrderDataEntity params) {
		return R.ok(payService.updateOrder(params));
	}

}
