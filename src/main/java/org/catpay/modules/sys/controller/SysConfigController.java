

package org.catpay.modules.sys.controller;


import org.catpay.common.annotation.SysLog;
import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.PageUtils;
import org.catpay.modules.sys.entity.SysConfigEntity;
import org.catpay.modules.sys.service.SysConfigService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 系统配置信息
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController extends AbstractController {
	
	@Autowired
	private SysConfigService sysConfigService;
	
	/**
	 * 所有配置列表
	 */
	@PostMapping("/list")
	//@RequiresPermissions("sys:config:list")
	public R<?> list(@RequestBody Map<String, Object> params){
		PageUtils page = sysConfigService.queryPage(params);
		return R.ok(CommonMap.getMap().put("page", page));
	}
	
	
	/**
	 * 配置信息
	 */
	@GetMapping("/info/{id}")
	//@RequiresPermissions("sys:config:info")
	public R<?> info(@PathVariable("id") Long id){
		SysConfigEntity config = sysConfigService.selectById(id);
		return R.ok(CommonMap.getMap().put("config", config));
	}
	
	/**
	 * 保存配置
	 */
	@SysLog("保存配置")
	@PostMapping("/save")
	//@RequiresPermissions("sys:config:save")
	public R<?> save(@RequestBody @Validated SysConfigEntity config){
		sysConfigService.save(config);
		return R.ok();
	}
	
	/**
	 * 修改配置
	 */
	@SysLog("修改配置")
	@PostMapping("/update")
	//@RequiresPermissions("sys:config:update")
	public R<?> update(@RequestBody @Validated SysConfigEntity config){
		sysConfigService.update(config);
		return R.ok();
	}
	
	/**
	 * 删除配置
	 */
	@SysLog("删除配置")
	@PostMapping("/delete")
	//@RequiresPermissions("sys:config:delete")
	public R<?> delete(@RequestBody Long[] ids){
		sysConfigService.deleteBatch(ids);
		return R.ok();
	}

}
