
package org.catpay.modules.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 系统页面视图
 * 

 */
@Controller
public class SysPageController {
	
	@RequestMapping("/modules/{module}/{url}")
	public String module(@PathVariable("module") String module, @PathVariable("url") String url){
		return "modules/" + module + "/" + url;
	}
	
	@RequestMapping("/modules/{module}/{url}.html")
	public String moduleV2(@PathVariable("module") String module, @PathVariable("url") String url){
		return "modules/" + module + "/" + url;
	}


	@RequestMapping("login.html")
	public String loginAndMain(){
		return "/"+"login";
	}
	
	@RequestMapping("/{module}/{url}.html")
	public String index(@PathVariable("url") String url){
		return "modules/"+url;
	}
}
