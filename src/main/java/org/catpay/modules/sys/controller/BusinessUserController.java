package org.catpay.modules.sys.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.catpay.common.exception.RRException;
import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.IncomeDataEntity;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.app.entity.WithdrawalDataEntity;
import org.catpay.modules.app.service.AccService;
import org.catpay.modules.app.service.PayService;
import org.catpay.modules.app.service.UserService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.map.MapUtil;

@RestController
@RequestMapping("/business")
public class BusinessUserController extends AbstractController{

	@Autowired
	private UserService userService;
	
	@Autowired
	private AccService accService;
	
	@Autowired
	private PayService payService;
	
	@PostMapping("/userList")
	public R<?> list(@RequestBody Map<String, Object> params){
		PageUtils page = userService.queryPage(params);
		return R.ok(CommonMap.getMap().put("page", page));
	}
	
	@GetMapping("/getUserInfo/{userId}")
	public R<?> getUserInfo(@PathVariable("userId") Long userId){
		return R.ok(userService.getUser(userId));
	}
	
	@PostMapping("/editLevel")
	public R<?> editLevel(@RequestBody Map<String, Object> params){
		Long userId= MapUtil.getLong(params, "userId");
		Integer level= MapUtil.getInt(params, "level");
		Integer role = MapUtil.getInt(params, "role");
		if(userId == null) 
			throw new RRException("用户ID不能为空");
		if(role == null )throw new RRException("角色不能为空");
		else {
			if(level == null)throw new RRException("等级不能为空");
			if(role == 0 && level!=0)throw new RRException("普通用户不能修改等级");
			if(role != 0 && level == 0)throw new RRException("供应商不能修改改等级");
		}
		
		
		UserEntity userEntity = new UserEntity();
		userEntity.setId(userId);
		userEntity.setLevel(level);
		userEntity.setRole(role);
		userEntity.updateById();
		return R.ok();
	}
	
	//获取提现记录
	@PostMapping("/getWithdrawalData") 
	public R<?> getWithdrawalData(@RequestBody Map<String, Object> params){
//		int pageNo = MapUtil.getInt(params, "pageNo");
//		if(pageNo == 5) return R.ok( new ArrayList<>());
//		int pageSzie = MapUtil.getInt(params, "pageSzie");
//		List<WithdrawalDataEntity> list = new ArrayList<>();
//		for(int i =0; i<pageSzie; i++) {
//			list.add(new WithdrawalDataEntity());
//		}
//		return R.ok(list);
		return R.ok(accService.getWithdrawalData(MapUtil.getLong(params, "userId"), MapUtil.getStr(params, "pageNo"), MapUtil.getStr(params, "pageSzie")));
	}
	
	//获取订单详情
	@PostMapping("/getOrder")
	public R<?> getOrder(@RequestBody Map<String, Object> params){
		return R.ok(payService.getOrder(MapUtil.getLong(params, "userId"), MapUtil.getStr(params, "status"), MapUtil.getStr(params, "pageNo"), MapUtil.getStr(params, "pageSzie")));
	}
	
	//获取收益明细
	@PostMapping("/getUserIncomeData")
	public R<?> getUserIncomeData(@RequestBody Map<String, Object> params){
		return R.ok(CommonMap.getMap().put("page", accService.queryUserIncomeDataPage(params)));
		
	}
	
}
