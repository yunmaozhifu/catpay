package org.catpay.modules.sys.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.catpay.common.exception.RRException;
import org.catpay.common.utils.PageUtils;
import org.catpay.common.utils.Query;
import org.catpay.modules.app.entity.ProductEntity;
import org.catpay.modules.app.service.impl.QiniuyunServiceImpl;
import org.catpay.modules.sys.dao.BusinessProductDao;
import org.catpay.modules.sys.service.BusinessProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;

@Service("businessProductService")
public class BusinessProductServiceImpl extends ServiceImpl<BusinessProductDao, ProductEntity> implements BusinessProductService{

	@Autowired
	private QiniuyunServiceImpl qiniuyunService;
	
	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String title = MapUtil.getStr(params, "title");
		Page<ProductEntity> page = this.selectPage(
				new Query<ProductEntity>(params).getPage(),
				new EntityWrapper<ProductEntity>().like(StrUtil.isNotBlank(title), "title", title).orderBy("isBannerProduct", false)
			);
			
			return new PageUtils(page);
	}

	@Override
	public void productSave(ProductEntity productEntity) {
		this.insert(productEntity);
	}

	@Override
	public List<ProductEntity> getBannerProduct() {
		return new ProductEntity().selectList(new EntityWrapper<ProductEntity>().
				eq("isGrounding", true).eq("isBannerProduct", true).orderDesc(Arrays.asList("pro_order")));
	}

	@Override
	public List<ProductEntity> getAllProduct() {
		return new ProductEntity().selectList(new EntityWrapper<ProductEntity>().eq("isGrounding", true).
				eq("isBannerProduct",false).orderDesc(Arrays.asList("pro_order")));
	}

	@Override

	public ProductEntity getProduct(Long id) {
		
		return this.selectById(id);

	}
	public String uploadFile(MultipartFile file,String type) {
		try {
			String url =  qiniuyunService.convertImageUrl(qiniuyunService.uploadMultipartFile(file, type));
			if(StrUtil.isNotBlank(url))
				return url;
			throw new RRException("上传失败！");
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new RRException("上传失败！");
		}

	}

}
