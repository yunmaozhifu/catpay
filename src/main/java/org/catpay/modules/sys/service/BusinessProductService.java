package org.catpay.modules.sys.service;

import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.ProductEntity;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.service.IService;

public interface BusinessProductService extends IService<ProductEntity>{

	PageUtils queryPage(Map<String, Object> params);

	void productSave(ProductEntity productEntity);
	
	//获取已上架的榜单产品
	List<ProductEntity> getBannerProduct();
	
	
	//获取所有已上架的产品（不包含榜单产品）
	List<ProductEntity> getAllProduct();
	
	//获取所有已上架的产品（不包含榜单产品）
	ProductEntity getProduct(Long id);

	String uploadFile(MultipartFile file,String type);

}
