package org.catpay.modules.sys.service;

import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.app.entity.UserEntity;

import com.baomidou.mybatisplus.service.IService;

public interface BusinessUserService extends IService<UserEntity>{
	
	PageUtils queryPage(Map<String, Object> params);
	

}
