
package org.catpay.modules.sys.service;


import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.sys.entity.SysLogEntity;

import com.baomidou.mybatisplus.service.IService;


/**
 * 系统日志
 * 
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
