package org.catpay.modules.sys.service;

import java.util.List;
import java.util.Map;

import org.catpay.common.utils.PageUtils;
import org.catpay.modules.sys.entity.SysRoleEntity;

import com.baomidou.mybatisplus.service.IService;



/**
 * 角色
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	void save(SysRoleEntity role);

	void update(SysRoleEntity role);

	void deleteBatch(Long[] roleIds);

	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);
}
