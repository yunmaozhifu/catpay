package org.catpay.modules.sys.service.impl;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.catpay.common.utils.PageUtils;
import org.catpay.common.utils.Query;
import org.catpay.modules.app.entity.UserEntity;
import org.catpay.modules.sys.dao.BusinessUserDao;
import org.catpay.modules.sys.service.BusinessUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

@Service("businessUserService")
public class BusinessUserServiceImpl extends ServiceImpl<BusinessUserDao, UserEntity> implements BusinessUserService{

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String phone = MapUtils.getString(params, "phone");
		Page<UserEntity> page = this.selectPage(
			new Query<UserEntity>(params).getPage(),
			new EntityWrapper<UserEntity>()
				.eq(StringUtils.isNotBlank(phone),"phone", phone)
		);
		
		return new PageUtils(page);
	}

}
