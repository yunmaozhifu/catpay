package org.catpay.modules.sys.service.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.catpay.common.utils.CommonMap;
import org.catpay.common.utils.JsonUtil;
import org.catpay.common.utils.RedisUtils;
import org.catpay.modules.sys.entity.SysUserEntity;
import org.catpay.modules.sys.entity.SysUserTokenEntity;
import org.catpay.modules.sys.oauth2.TokenGenerator;
import org.catpay.modules.sys.service.SysUserTokenService;
import org.catpay.modules.sys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("sysUserTokenService")
public class SysUserTokenServiceImpl implements SysUserTokenService {

	@Value("${base.token.expire: 43200}")
	private Long EXPIRE;

	@Autowired
	private RedisUtils redisUtils;

	@Override
	public R<?> createToken() {
		SysUserEntity curUserEntity = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
		
		Long userId = curUserEntity.getUserId();
		String result = redisUtils.get(String.valueOf(userId));
		
		// 当前时间
		Date now = new Date();
		// 过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);
		if(StringUtils.isNotBlank(result)) {
			SysUserTokenEntity sysUserTokenEntity = JsonUtil.getInstance().json2obj(result, SysUserTokenEntity.class);
			sysUserTokenEntity.setExpireTime(expireTime);
			sysUserTokenEntity.setUpdateTime(now);
			redisUtils.set(String.valueOf(userId),sysUserTokenEntity, EXPIRE);
			
			return R.ok(CommonMap.getMap().put("token", sysUserTokenEntity.getToken()).put("expire", expireTime));
		}
		
		// 生成一个token
		String token = TokenGenerator.generateValue();
		
		SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(expireTime);
		redisUtils.set(String.valueOf(userId),tokenEntity, EXPIRE);

		return R.ok(CommonMap.getMap().put("token", token).put("expire", expireTime));

	}

	@Override
	public void logout(String userId) {
		redisUtils.delete(userId);
	}
}
