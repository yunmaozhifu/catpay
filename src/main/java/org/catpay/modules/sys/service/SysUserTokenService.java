package org.catpay.modules.sys.service;

import org.catpay.modules.sys.utils.R;

/**
 * 用户Token
 * 
 */
public interface SysUserTokenService{

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	R<?> createToken();

	/**
	 * 退出
	 */
	void logout(String userId);

}
