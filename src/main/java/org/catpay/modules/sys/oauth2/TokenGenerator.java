package org.catpay.modules.sys.oauth2;

import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.catpay.common.exception.RRException;

/**
 * 生成token
 *
 */
public class TokenGenerator {

    public static String generateValue() {
        return generateValue(UUID.randomUUID().toString());
    }

    private static final char[] hexCode = "0123456789abcdef".toCharArray();

    public static String toHexString(byte[] data) {
        if(data == null) {
            return null;
        }
        StringBuilder r = new StringBuilder(data.length*2);
        for ( byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }

    public static String generateValue(String param) {
        try {
            return toHexString(DigestUtils.md5(param));
        } catch (Exception e) {
            throw new RRException("生成Token失败", e);
        }
    }
}
