
package org.catpay.modules.sys.form;

import java.awt.SecondaryLoop;
import java.io.Serializable;

import javax.validation.constraints.NotBlank;


import lombok.Data;
import net.sf.jsqlparser.statement.select.First;

/**
 * 登录表单
 *
 */
@Data
public class SysLoginForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2722109891910888185L;
	@NotBlank(message="用户名不能为空")
    private String username;
	@NotBlank(message="密码不能为空")
    private String password;
	@NotBlank(message="验证码不能为空")
    private String captcha;
	@NotBlank(message="uuid不能为空")
    private String uuid;
   
}
