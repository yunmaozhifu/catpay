
package org.catpay.modules.sys.form;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * 密码表单
 *
 */
@Data
public class PasswordForm {
    /**
     * 原密码
     */
	@NotBlank(message="原密码不能为空")
    private String password;
    /**
     * 新密码
     */
	@NotBlank(message="新密码不能为空")
    private String newPassword;

}
