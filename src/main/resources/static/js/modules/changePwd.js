layui.use(['form'],function(){
    var form = layui.form,
    	$ = layui.jquery;

    var curUser = $.parseJSON(sessionStorage.getItem("curUser"));
    $(".username").val(curUser.username);
    
    form.verify({
        newPwd : function(value, item){
            if(value.length < 6){
                return "密码长度不能小于6位";
            }
        },
        confirmPwd : function(value, item){
            if(!new RegExp($("#oldPwd").val()).test(value)){
                return "两次输入密码不一致，请重新输入！";
            }
        }
    })
    
    //修改密码
    form.on("submit(changePwd)",function(data){
    	ajaxRequest({
    		url:'/sys/user/password',
    		param:data.field,
    		needLoader:true,
    		success:function(){
    			$("button[type=reset]").click();
    			baseAlert("修改成功");
    		}
    	})
    	
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    })
})