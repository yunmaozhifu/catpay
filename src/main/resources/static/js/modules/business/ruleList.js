layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var editId,curUser;
layui.use(['form','layer',"baseTable"],function(){
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laytpl = layui.laytpl,
		table = layui.baseTable;

	curUser =  $.parseJSON(sessionStorage.getItem("curUser"));



	//用户列表
	var tableIns = table.render({
		elem: '#ruleList',
		url : baseUrl+'/business/rule/list',
		cellMinWidth : 95,
		id : "ruleListTable",
		cols : [[
			{field: 'id', title: 'id',hide:true, minWidth:100, align:"center"},
			{field: 'goodsId', title: '产品ID',hide:true,width:250, align:"center"},
			{field: 'title', title: '产品',width:250, align:"center"},
			{field: 'cond', title: '数量', width:100, align:'center'},
			{field: 'userChose', title: '奖励对象', width:300, align:'center',templet:function(d){
				switch(d.userChose){
					case 1:
						return '<label class="layui-btn-danger layui-btn-xs layui-btn" style="cursor:auto">本人</label>';
					case 2:
						return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">推荐人</label>';
					case 3:
						return '<label class="layui-btn-blue layui-btn-xs layui-btn" style="cursor:auto">推荐人的推荐人</label>';
				}
			}},
			{field: 'toPatherRole', title: '被奖励角色', width:100, align:'center',templet:function(d){
				switch(d.toPatherRole) {
					case 0:
						return '<label class="layui-btn-danger layui-btn-xs layui-btn" style="cursor:auto">普通会员</label>';
					case 1:
						return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">提供商</label>';
				}
				}},
			{field: 'toPatherLevel', title: '等级', width:100, align:'center',templet:function(d){
				switch(d.toPatherLevel){
					case 0:
						return '<label class="layui-btn-danger layui-btn-xs layui-btn" style="cursor:auto">无</label>';
					case 1:
						return '<label class="layui-btn-danger layui-btn-xs layui-btn" style="cursor:auto">初级</label>';
					case 2:
						return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">中级</label>';
					case 3:
						return '<label class="layui-btn-blue layui-btn-xs layui-btn" style="cursor:auto">高级</label>';
				}
			}},
			{field: 'aumt', title: '奖励金额', width:100, align:'center'},
			{field: 'status', title: '状态', width:100, align:'center',templet:function(d){
				switch(d.status){
					case -1:
						return '<label class="layui-btn-danger layui-btn-xs layui-btn" style="cursor:auto">停用</label>';
					default:
						return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">正常</label>';
				}
			}},
			{title: '操作', templet:'#ruleListBar',align:"center"}
		]],
		done:function(){
			//init();
			if(curUser.userId != 1){
				$(".del,.delAll_btn").remove();
			}


		}
	});
	//搜索【此功能需要后台配合，所以暂时没有动态效果演示】
	$(".search_btn").on("click",function(){
		tableIns.reload({
			where: {
				paramKey: $(".searchVal").val()  //搜索的关键字
			}
		})

	});

	$(".addRule_btn").click(function(){
		addRule();
	})

	//添加用户
	function addRule(edit){
		var index = layui.layer.open({
			title : "添加奖励规则",
			type : 2,
			content : getUrl("ruleAdd.html"),
			success : function(layero, index){
				setTimeout(function(){
					layui.layer.tips('点击此处返回奖励规则列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},500)
			}
		})
		layui.layer.full(index);
		window.sessionStorage.setItem("index",index);
		//改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
		$(window).on("resize",function(){
			layui.layer.full(window.sessionStorage.getItem("index"));
		})
	}


	function editRule(edit){

		editId = edit.id;
		var index = layui.layer.open({
			title : "编辑奖励规则",
			type : 2,
			content : getUrl("ruleEdit.html"),
			success : function(layero, index){
				setTimeout(function(){
					layui.layer.tips('点击此处返回奖励规则列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},500)
			}
		})
		layui.layer.full(index);
		window.sessionStorage.setItem("index",index);
		//改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
		$(window).on("resize",function(){
			layui.layer.full(window.sessionStorage.getItem("index"));
		})
	}





	//列表操作
	table.on('tool(ruleList)', function(obj){
		var layEvent = obj.event,
			data = obj.data;
		if(layEvent === 'edit'){ //编辑
			editRule(data);
		}else if(layEvent === 'enable'){
			layer.confirm('确定启用此规则？',{icon:3, title:'提示信息'},function(index){
				ajaxRequest({
					url:'/business/rule/enable',
					param:data,
					success:function(){
						table.reload("ruleListTable");
						baseAlert("启用成功");
					}
				})
				layer.close(index);

			});
		}else if(layEvent === 'disable'){
			layer.confirm('确定停用此规则？',{icon:3, title:'提示信息'},function(index){
				ajaxRequest({
					url:'/business/rule/disable',
					param:data,
					success:function(){
						table.reload("ruleListTable");
						baseAlert("停用成功");
					}
				})
				layer.close(index);

			});
		}

	});


})
