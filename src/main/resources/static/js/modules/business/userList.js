layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var editId,curUser,phone,level,role;
layui.use(['form','layer',"baseTable"],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.baseTable;
    
         curUser =  $.parseJSON(sessionStorage.getItem("curUser"));
         
         
         
    //用户列表
    var tableIns = table.render({
        elem: '#userList',
        url : baseUrl+'/business/userList',
        cellMinWidth : 95,
        id : "userListTable",
        cols : [[
        	{field:"1", type: "checkbox", width:50},
            {field: 'id', title: '用户ID',hide:true, minWidth:100, align:"center"},
            {field: 'nickName', title: '昵称',width:150, align:"center"},
            {field: 'phone', title: '电话号码', align:'center',width:150},
            {field: 'role', title: '用户类型', width:100, align:'center',templet:function(d){
                return d.role == 0? '普通用户':'供应商';
            }},
            {field: 'level', title: '等级', align:'center',width:100,templet:function(d){
            	switch(d.level){
             	case 0:
            		return '普通用户'
            	case 1:
            		return '中级供应商'
            	case 2:
            		return '高级供应商'
            	case 3:
            		return '城市合伙人'
            	}
       
            }},
            
            {field: 'accountBalance', title: '余额', width:100, align:'center'},
            {field: 'profitBalance', title: '收益', width:100, align:'center'},
            {field: 'frozenBalance', title: '冻结金额',width:100, align:'center'},
            {field: 'status', title: '账号状态',  align:'center',width:100,templet:function(d){
                return d.status == 0 ? '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">'+"正常"+'</label>' : '<label class="layui-bg-red layui-btn-xs layui-btn" style="cursor:auto">'+"冻结"+'</label>';
            }},
            {field: 'createTime', title: '创建时间', align:'center',width:180},
            {title: '操作', templet:'#userListBar',align:"center"}
        ]],
        done:function(){
        	//init();
        	if(curUser.userId != 1){
        		$(".del,.delAll_btn").remove();
        	}
        	
        	
        }
    });
    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".search_btn").on("click",function(){
       
        	tableIns.reload({
        		where: {
                    phone: $(".searchVal").val()  //搜索的关键字
                }
        	})
        
    });

    //添加用户
    function addSharePage(edit){
    	var checkStatus = table.checkStatus('userListTable')
    	var d = checkStatus.data;
    	if(d.length == 1){
    		editId = d[0].id;
    		phone  = d[0].phone;
    		 var index = layui.layer.open({
    	            title : "添加分享页",
    	            area: ['580px', '650px'],
    	            type : 2,
    	            content : getUrl("addSharePage.html"),
    	            success : function(layero, index){
    	            	
    	            }
    	        })
    	}else{
    		layer.msg("请选择一个用户");
    	}
    		
    	
    	
       
//        layui.layer.full(index);
//        window.sessionStorage.setItem("index",index);
//        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
//        $(window).on("resize",function(){
//            layui.layer.full(window.sessionStorage.getItem("index"));
//        })
    }
    
    function orderView(data){
    	editId = data.id;
    	var index = layui.layer.open({
            title : "个人订单",
            type : 2,
            area: ['900px', '500px'],
            content : getUrl("personOrder.html"),
            success : function(layero, index){
            }
        })
    }
    
    function incomeView(data){
    	editId = data.id;
    	var index = layui.layer.open({
            title : "收益记录",
            type : 2,
            area: ['900px', '500px'],
            content : getUrl("incomeView.html"),
            success : function(layero, index){
//              setTimeout(function(){
//                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
//                        tips: 3
//                    });
//                },500)
            }
        })
    }
    function cashView(data){
    	editId = data.id;
    	var index = layui.layer.open({
            title : "提现记录",
            type : 2,
            area: ['900px', '500px'],
            content : getUrl("cashView.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
   //     layui.layer.full(index);
//        window.sessionStorage.setItem("index",index);
//        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
//        $(window).on("resize",function(){
//            layui.layer.full(window.sessionStorage.getItem("index"));
//        })
    	
    }
    
    
    function viewUser(data){
    	editId = data.id;
        var index = layui.layer.open({
            title : "查看详情",
            type : 2,
            content : getUrl("userView.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
   //     window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
//        $(window).on("resize",function(){
//            layui.layer.full(window.sessionStorage.getItem("index"));
//        })
    }
    
    $(".addSharePage_btn").click(function(){
        addSharePage();
    })
    
      $(".editLevel_btn").click(function(){
    	  var checkStatus = table.checkStatus('userListTable')
      	var d = checkStatus.data;
      	if(d.length == 1){
//      		if(d[0].role == 0){
//      			layer.msg("普通用户不能修改等级");
//      			return;
//      		}
//      		
      		editId = d[0].id;
      		level = d[0].level
      		role = d[0].role
      		 var index = layui.layer.open({
      	            title : "修改用户等级",
      	            area: ['500px', '300px'],
      	            type : 2,
      	            content : getUrl("editLevel.html"),
      	            success : function(layero, index){
      	            	
      	            }
      	        })
      	}else{
      		layer.msg("请选择一个用户");
      	}
    })

    

    //列表操作
    table.on('tool(userList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'view'){ //编辑
            viewUser(data);
        }else if(layEvent === 'cash'){
        	cashView(data)
        }else if(layEvent == 'income'){
        	incomeView(data)
        }else if(layEvent == 'order'){
        	orderView(data)
        }
        
    });

   
})
