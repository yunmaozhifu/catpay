layui.use(['form','layer','upload','layedit'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        		upload = layui.upload,
        	 layedit = layui.layedit,
        $ = layui.jquery;
    
    layedit.set({
    	  uploadImage: {
    	    url: baseUrl+'/business/uploadDetail' //接口url
    	    ,type: 'post' //默认post
    	  }
    	})
    var editIndex =layedit.build('details');
    $(function(){
    	let proId = parent.editId;
    	ajaxRequest({
    		url:'/business/getProductInfo/'+proId,
    		type:'get',
    		success:function(data){
    			$(".title").val(data.title);
    			$(".subtitle").val(data.subtitle);
    			$(".price").val(data.price);
    			$(".order").val(data.order);
    			layedit.setContent(editIndex,data.details)
    			$("input[name='type'][value="+data.type+"]").attr("checked",true);
    			$(".imgUrl").attr("src",data.imgUrl);
    			form.render();
    		}
    	})
    })
    var index = null;
    var uploadInst = upload.render({
	    elem: '#test10'
	    ,url: baseUrl+'/business/upload'
	    ,before: function(obj){
	    	index = top.layer.msg('上传中，请稍候',{icon: 16,time:false,shade:0.8});
	      }
	    ,done: function(res){
	    	top.layer.close(index);
	    	 if(res.code == 200){
	        	 $(".imgUrl").attr("src",res.result.url);
	            return layer.msg('上传成功');
	          }else{
	        	  return layer.msg('上传失败');
	          }
	    }
	  });
    
    form.on("submit(editProduct)",function(data){
    	data.field.imgUrl = $(".imgUrl").attr("src");
    	data.field.id = parent.editId;
    	data.field.details = layedit.getContent(editIndex)
    	console.log(layedit.getContent(editIndex))
        ajaxRequest({
        	url:'/business/productUpdate',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("修改成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

})