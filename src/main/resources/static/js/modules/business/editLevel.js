layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
    	let level = parent.level;
    	let role = parent.role;
    	if(role == 0){
    		$("select[name='level']").attr("disabled",true);
    		$("select[name='level']").append("<option value='0'>普通用户</option>");
    	}
    	$("select[name='role']").find("option[value="+role+"]").attr("selected",true);
    	$("select[name='level']").find("option[value="+level+"]").attr("selected",true);
    	form.render();
    })
    
 
			form.on('select(aihao1)', function(data){
				if(data.value == 0){
					if(!$("select[name='level']").find("option[value=0]").val()){
						$("select[name='level']").append("<option value='0'>普通用户</option>");
						$("select[name='level']").find("option[value=0]").attr("selected",true);
					}
					$("select[name='level']").attr("disabled",true);
				}else{
					$("select[name='level']").attr("disabled",false);
					$("select[name='level'] option[value=0]").remove();
				}
				
				
				form.render();
			});

  
    
    form.on("submit(editLevel)",function(data){
    	data.field.userId = parent.editId;
        ajaxRequest({
        	url:'/business/editLevel',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("修改成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	}
        })
        
        return false;
    })

   

})