layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
layui.use(['form','layer','flow','laydate',"baseTable"],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        table = layui.baseTable,
        flow = layui.flow;

    $(function(){
    	let userId = parent.editId;
    	 laydate.render({
    		    elem: '#test6'
    		    ,range: true
    		  });
    	
    	 
    	 $(".search_btn").click(function(){
    		 var dateTime = $("#test6").val();
    		 if(dateTime){
    			 var tmp = dateTime.split(' - ');
    			 table.render({
    			        elem: '#incomeList',
    			        url : baseUrl+'/business/getUserIncomeData',
    			        cellMinWidth : 95,
    			        id : "incomeListTable",
    			        where:{userId:userId,startTime:tmp[0],endTime:tmp[1]},
    			        cols:[[
    			            {field: 'id', title: 'id',hide:true, minWidth:100, align:"center"},
    			            {field: 'amount', title: '收益金额',width:100, align:"center"},
    			            {field: 'type', title: '收益类型', align:'center',width:100,templet:function(d){
    			            	switch(d.type){
    			            		case '1':
    			            			return '激活POS机'
    			            		case '2':
    			            			return '刷卡'
    			            		case '3':
    			            			return '发展下级'
    			            		default:
    			            			return '未定义类型'
    			            	}
    			            }},
    			            {field: 'createTime', title: '创建时间', align:'center',width:180},
    			            {field: 'remark', title: '备注', align:'center',width:180}
    			        ]]
    			    });    			 
    		 }else{
    			 layer.msg('请选择日期范围');
    		 }
    	 })
    	 
 
    })
    
    
    

   

})