layui.use(['form','layer','upload','layedit'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        		upload = layui.upload,
        		 layedit = layui.layedit,
        $ = layui.jquery;

    layedit.set({
  	  uploadImage: {
  	    url: baseUrl+'/business/uploadDetail' //接口url
  	    ,type: 'post' //默认post
  	  }
  	})
  var editIndex =layedit.build('details');
    form.on("submit(addProduct)",function(data){
        //弹出loading
    	
//    	     var arr = new Array();
//    	      $("input:checkbox[name='roleIdList']:checked").each(function(i){
//    	                arr[i] = $(this).val();
//    	        });
//    	data.field.roleIdList = arr;
    	if(data.field.isBannerProduct == '0')
    		data.field.bannerProduct = false
    	else
    		data.field.bannerProduct = true
    	if(data.field.isGrounding == '0')
        	data.field.grounding = false
        else
        	data.field.grounding = true
        	
        data.field.details = layedit.getContent(editIndex)	
        ajaxRequest({
        	url:'/business/productSave',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("添加成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })
    
    var index = null;
    var uploadInst = upload.render({
	    elem: '#test1'
	    ,url: baseUrl+'/business/upload'
	    ,before: function(obj){
	    	index = top.layer.msg('上传中，请稍候',{icon: 16,time:false,shade:0.8});
	      }
	    ,done: function(res){
	    	top.layer.close(index);
	    	 if(res.code == 200){
	    		 $(".imgUrl").val(res.result.url);
	        	 $(".proUrl").attr("src",res.result.url);
	            return layer.msg('上传成功');
	          }else{
	        	  return layer.msg('上传失败');
	          }
	    }
	  });

//    var uploadInst = upload.render({
//        elem: '#test1'
//        ,url: baseUrl+'/business/upload'
//        ,before: function(obj){
//          //预读本地文件示例，不支持ie8
//          obj.preview(function(index, file, result){
//            $('#demo1').attr('src', result); //图片链接（base64）
//          });
//        }
//        ,done: function(res){
//        	console.log(res)
//          //如果上传失败
//          if(res.code == 200){
//        	  $(".imgUrl").val(res.result.url);
//            return layer.msg('上传成功');
//          }else{
//        	  return layer.msg('上传失败');
//          }
//          //上传成功
//        }
//        ,error: function(){
//          //演示失败状态，并实现重传
//          var demoText = $('#demoText');
//          demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
//          demoText.find('.demo-reload').on('click', function(){
//            uploadInst.upload();
//          });
//        }
//      });

})