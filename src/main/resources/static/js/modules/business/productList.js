layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var editId,curUser;
layui.use(['form','layer',"baseTable"],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.baseTable;
    
         curUser =  $.parseJSON(sessionStorage.getItem("curUser"));
         
         
         
    //用户列表
    var tableIns = table.render({
        elem: '#productList',
        url : baseUrl+'/business/productList',
        cellMinWidth : 95,
        id : "productListTable",
        cols : [[
        	{field:"id", title:'id',hide:true, minWidth:100, align:"center"},
            {field:"1", type: "checkbox", width:50},
            {field: 'title', title: '标题', minWidth:100, align:"center"},
            {field: 'subtitle', title: '副标题', minWidth:100, align:"center"},
            {field: 'price', title: '价格', align:'center'},
            {field: 'order', title: '序号',  align:'center'},
            {field: 'type', title: '类型',  align:'center',templet:function(d){
                return d.type == 1 ? '实物':'虚拟';
            }},
            {field: 'imgUrl', title: '图片',  align:'center',templet:function(d){
            	if(d.imgUrl)
            		return "<img width='60' height='60' src="+d.imgUrl+"></img>"
            	return ''
            }},
            {field: 'bannerProduct', title: '是否榜单产品',  align:'center',templet:function(d){
                return d.bannerProduct? '是':'否';
            }},
            {field: 'grounding', title: '上下架',  align:'center',templet:function(d){
                return d.grounding? '上架':'下架';
            }},
            {field: 'createTime', title: '创建时间', align:'center',minWidth:150},
            {title: '操作', minWidth:175, templet:'#productListBar',align:"center"}
        ]],
        done:function(){
        	//init();
        	if(curUser.userId != 1){
        		$(".del,.delAll_btn").remove();
        	}
        	
        	
        }
    });
    $(".search_btn").on("click",function(){
        
    	tableIns.reload({
    		where: {
                title: $(".searchVal").val()  //搜索的关键字
            }
    	})
    
});

    //添加用户
    function addProduct(edit){
        var index = layui.layer.open({
            title : "添加产品",
            type : 2,
            content : getUrl("productAdd.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
      //  window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
    }
    
    function editPro(edit){
    	editId = edit.id;
        var index = layui.layer.open({
            title : "编辑产品",
            type : 2,
          //  area: ['580px', '650px'],
            content : getUrl("productEdit.html"),
            success : function(layero, index){
            }
        })
        layui.layer.full(index);
    }
    
    $(".addProduct_btn").click(function(){
        addProduct();
    })

    $(".reset_btn").click(function(){
    	  var checkStatus = table.checkStatus('userListTable'),
            data = checkStatus.data,
            userIds = [];
    	  if(data.length > 0) {
              for (var i in data) {
              	userIds.push(data[i].userId);
              }
              layer.confirm('确定重置选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
              	ajaxRequest({
              		url:'/sys/user/resetPwd',
              		param:userIds,
              		success:function(){
              			baseAlert("重置成功，密码为123456");
              		}
              	})
              	
              })
          }else{
              layer.msg("请选择需要重置的用户");
          }
    })
    
    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('userListTable'),
            data = checkStatus.data,
            userIds = [];
        if(data.length > 0) {
            for (var i in data) {
            	userIds.push(data[i].userId);
            }
            layer.confirm('确定删除选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
            	ajaxRequest({
            		url:'/sys/user/delete',
            		param:userIds,
            		success:function(){
            			table.reload("userListTable");
            			baseAlert("删除成功")
            		}
            	})
            	layer.close(index);
            	
            })
        }else{
            layer.msg("请选择需要删除的用户");
        }
    })

    //列表操作
    table.on('tool(productList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
            editPro(data);
        }else if(layEvent === 'usable'){ //启用禁用
            var _this = $(this),
                usableText = "是否确定设置为榜单产品？",
                status =true ,
                btnText = "下架榜单产品";
            if(_this.text()=="下架榜单产品"){
                usableText = "是否确定下架该榜单产品？",
                btnText = "设置为榜单产品";
                status = false;
            }
            layer.confirm(usableText,{
                icon: 3,
                title:'系统提示',
                cancel : function(index){
                    layer.close(index);
                }
            },function(index){
            	ajaxRequest({
            		url:'/business/setBanner',
            		param:{id:data.id,bannerProduct:status},
            		success:function(){
            			obj.update({
            				'bannerProduct': status
            				});
            			_this.text(btnText).toggleClass("layui-btn-warm").toggleClass("layui-btn-danger");
            		}
            	})
                layer.close(index);
            },function(index){
                layer.close(index);
            });
        }else if(layEvent === 'del'){ //删除
        	  var _this = $(this),
              usableText = "确定上架该产品？",
              status =true ,
              btnText = "下架";
          if(_this.text()=="下架"){
              usableText = "确定下架该产品？",
              btnText = "上架";
              status = false;
          }
          layer.confirm(usableText,{
              icon: 3,
              title:'系统提示',
              cancel : function(index){
                  layer.close(index);
              }
          },function(index){
          	ajaxRequest({
          		url:'/business/setGrounding',
          		param:{id:data.id,grounding:status},
          		success:function(){
          			obj.update({
          				'grounding': status
          				});
          			_this.text(btnText).toggleClass("layui-btn-warm").toggleClass("layui-btn-danger");
          		}
          	})
              layer.close(index);
          },function(index){
              layer.close(index);
          });
        }
    });

   
})
