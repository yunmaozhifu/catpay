layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var shareId,pic;
layui.use(['form','layer',"baseTable"],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.baseTable;
    
         curUser =  $.parseJSON(sessionStorage.getItem("curUser"));
         
         //列表操作
         table.on('tool(sharePageList)', function(obj){
             var layEvent = obj.event,
                 data = obj.data;
             if(layEvent === 'edit'){ //编辑
            	 
            	 shareId = data.id;
            	 pic = data.img
                    var index = layui.layer.open({
                        title : "修改",
                        type : 2,
                        content : getUrl("editSharePage.html"),
                        success : function(layero, index){
                        }
                    })
                    layui.layer.full(index);
            	 
             }
             
             
         });    
         
    //用户列表
    var tableIns = table.render({
        elem: '#sharePageList',
        url : baseUrl+'/business/sharePageList',
        cellMinWidth : 95,
        id : "sharePageListTable",
        cols : [[
            {field: 'img', title: '地址', minWidth:100, align:"center"},
            {title: '操作', templet:'#sharePageListBar',align:"center"}
        ]],
        done:function(){
        	//init();
        	if(curUser.userId != 1){
        		$(".del,.delAll_btn").remove();
        	}
        	
        	
        }
    });



    	
    	


    
    


//      $(".editLevel_btn").click(function(){
//    	  var checkStatus = table.checkStatus('userListTable')
//      	var d = checkStatus.data;
//      	if(d.length == 1){
////      		if(d[0].role == 0){
////      			layer.msg("普通用户不能修改等级");
////      			return;
////      		}
////      		
//      		editId = d[0].id;
//      		level = d[0].level
//      		role = d[0].role
//      		 var index = layui.layer.open({
//      	            title : "修改用户等级",
//      	            area: ['500px', '300px'],
//      	            type : 2,
//      	            content : getUrl("editLevel.html"),
//      	            success : function(layero, index){
//      	            	
//      	            }
//      	        })
//      	}else{
//      		layer.msg("请选择一个用户");
//      	}
//    })

    



   
})
