layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){

		ajaxRequest({
			url:'/business/rule/getRuleGoods',
			type:'post',
			success:function(data){
				var proHtml = '';
				for (var i = 0; i < data.ruleGoodsList.length; i++) {
					proHtml += '<option value="' + data.ruleGoodsList[i].id + '">' + data.ruleGoodsList[i].title + '</option>';
				}
				$("select[name=goodsId]").append(proHtml);
				//form.render();

				let id = parent.editId;
				ajaxRequest({
					url:'/business/rule/info/'+id,
					type:'get',
					success:function(data){
						$("select[name='goodsId']").find("option[value="+data.incomeRule.goodsId+"]").attr("selected",true);
						$("select[name=cond]").val(data.incomeRule.cond);
						$("select[name=userChose]").val(data.incomeRule.userChose);
						$("select[name=toPatherRole]").val(data.incomeRule.toPatherRole);
						$("select[name=toPatherLevel]").val(data.incomeRule.toPatherLevel);
						$(".aumt").val(data.incomeRule.aumt);
						form.render();
					}
				})

			}
		})



    })
    
    
    
    form.on("submit(editRule)",function(data){
    	
    	   
    	data.field.id = parent.editId;
        ajaxRequest({
        	url:'/business/rule/update',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("修改成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

   

})