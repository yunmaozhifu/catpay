layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
    	let orderId = parent.editId;
    	ajaxRequest({
    		url:'/business/getOrderInfo/'+orderId,
    		type:'get',
    		success:function(data){
    			$(".orderNo").val(data.orderNo);
    			$(".amount").val(data.amount);
    			$(".goodsName").val(data.goodsName);
    			$(".totalCount").val(data.totalCount);
    			$(".goodsPrice").val(data.goodsPrice);
    			$(".goodsType").val(data.goodsType==1?'虚拟':'实物');
    			$(".nickName").val(data.nickName);
    			$(".address").val(data.province+
    					data.city+
    					data.area
    					+data.address);
    			var tmp ="";
    			switch(data.status){
        		case 1:
        			tmp = "待付款";
        			break;
        		case 2:
        			tmp = "待发货";
        			break;
        		case 3:
        			tmp = "待收货";
        			break;
        		case 4:
        			tmp = "已完成";
        			break;
        		case 5:
        			tmp = "已失效";
        			break;
        		case 6:
        			tmp = "已取消";
        			break;
        		case 7:
        			tmp = "申请退款";
        			break;
        		case 8:
        			tmp = "已退款";
        			break;
        	}
    			
    			$(".status").val(tmp)
    			
    			form.render();
    		}
    	})
    })
   

})