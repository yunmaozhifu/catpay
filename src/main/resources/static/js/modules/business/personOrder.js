layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
layui.use(['form','layer','flow','laydate',"baseTable",'element'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        table = layui.baseTable,
        flow = layui.flow,
        element = layui.element;

    $(function(){
    	let userId = parent.editId;
    	
    	 table.render({
		        elem: '#list1',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list1",
		        where:{userId:userId,status:99},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 
    	 table.render({
		        elem: '#list2',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list2",
		        where:{userId:userId,status:1},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 
    	 table.render({
		        elem: '#list3',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list3",
		        where:{userId:userId,status:2},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 table.render({
		        elem: '#list4',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list4",
		        where:{userId:userId,status:3},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 table.render({
		        elem: '#list5',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list5",
		        where:{userId:userId,status:4},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 table.render({
		        elem: '#list6',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list6",
		        where:{userId:userId,status:5},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
 
    	 table.render({
		        elem: '#list7',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list7",
		        where:{userId:userId,status:6},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 table.render({
		        elem: '#list8',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list8",
		        where:{userId:userId,status:7},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    	 table.render({
		        elem: '#list9',
		        url : baseUrl+'/business/orderList',
		        cellMinWidth : 95,
		        id : "list9",
		        where:{userId:userId,status:8},
		        cols:[[
		            {field: 'orderNo', title: '订单号',width:300, align:"center"},
		            {field: 'amount', title: '订单金额',width:100, align:"center"},
		            {field: 'goodsName', title: '商品名',width:200, align:"center"},
		            {field: 'totalCount', title: '商品数量',width:100, align:"center"},
		            {field: 'status', title: '订单状态',width:100, align:"center",templet:function(d){
		            	switch(d.status){
	            		case 1:
	            			return '待付款';
	            		case 2:
	            			return '待发货';
	            		case 3:
	            			return '待收货';
	            		case 4:
	            			return '>已完成';
	            		case 5:
	            			return '已失效';
	            		case 6:
	            			return '已取消';
	            		case 7:
	            			return '申请退款';
	            		case 8:
	            			return '已退款';
	            	}
	            	
	            }}
		        ]]
		    });
    })
    
    
    

   

})