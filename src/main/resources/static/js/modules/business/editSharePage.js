layui.use(['form','layer','upload'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        		upload = layui.upload,
        $ = layui.jquery;

    
    var img = parent.pic;
    var shareId = parent.shareId;
    $(function(){
    	$("#pic").attr("src",img);
    })
    
    var uploadInst = upload.render({
	    elem: '#test10'
	    ,url: baseUrl+'/business/editSharePage?&shareId='+shareId
	    ,before: function(obj){
	    	index = top.layer.msg('上传中，请稍候',{icon: 16,time:false,shade:0.8});
	      }
	    ,done: function(res){
	    	top.layer.close(index);
	    	 if(res.code == 200){
	        	 $(".imgUrl").attr("src",res.result.url);
	        	 parent.location.reload();
	            return layer.msg('修改成功');
	          }else{
	        	  return layer.msg('修改失败');
	          }
	    }
	  });
    
   

})