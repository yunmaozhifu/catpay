layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
        ajaxRequest({
            url:'/business/rule/getRuleGoods',
            type:'post',
            success:function(data){
                var proHtml = '';
                for (var i = 0; i < data.ruleGoodsList.length; i++) {
                    proHtml += '<option value="' + data.ruleGoodsList[i].id + '">' + data.ruleGoodsList[i].title + '</option>';
                }
                //初始化省数据
                $("select[name=goodsId]").append(proHtml);
                form.render();
            }
        })
    })

    form.on("submit(addRule)",function(data){
        //弹出loading
        ajaxRequest({
        	url:'/business/rule/save',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("奖励规则添加成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

   

})