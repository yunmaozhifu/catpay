layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
    	let orderId = parent.editId;
    	ajaxRequest({
    		url:'/business/getOrderInfo/'+orderId,
    		type:'get',
    		success:function(data){
    			$(".amount").val(data.amount).attr("disabled",true);
    			$(".totalCount").val(data.totalCount).attr("disabled",true);
    			$(".phone").val(data.phone).attr("disabled",true);
    			$(".nickName").val(data.nickName).attr("disabled",true);
    			$("select[name='status']").find("option[value="+data.status+"]").attr("selected",true);
    			if(data.status == 1 || data.status == 2){
    				$(".totalCount").attr("disabled",false)
    				$(".phone").attr("disabled",false)
    				$(".nickName").attr("disabled",false)
    			}
    			if(data.status == 1 || data.status == 7)
    				$(".amount").attr("disabled",false)
    			form.render();
    		}
    	})
    })
    
    
    
    form.on("submit(editOrder)",function(data){
    	data.field.orderNo = parent.editId;
        ajaxRequest({
        	url:'/business/update',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("修改成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	}
        })
        
        return false;
    })

   

})