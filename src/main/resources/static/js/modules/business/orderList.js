layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var editId,curUser;
layui.use(['form','layer',"baseTable"],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.baseTable;
    
         curUser =  $.parseJSON(sessionStorage.getItem("curUser"));
         
         
         
    //用户列表
    var tableIns = table.render({
        elem: '#orderList',
        url : baseUrl+'/business/orderList',
        cellMinWidth : 95,
        id : "orderListTable",
        cols : [[
            {field: 'id', title: 'id',hide:true, minWidth:100, align:"center"},
            {field: 'orderNo', title: '订单号',width:150, align:"center"},
            {field: 'nickName', title: '收货人', align:'center',width:150},
            {field: 'phone', title: '收货人电话', align:'center',width:150},
            {field: 'amount', title: '订单金额', width:100, align:'center'},
            {field: 'totalCount', title: '数量', width:100, align:'center'},
            {field: 'status', title: '订单状态', width:100, align:'center',templet:function(d){
            	switch(d.status){
            		case 1:
            			return '<label class="layui-btn-danger layui-btn-xs layui-btn" style="cursor:auto">待付款</label>';
            		case 2:
            			return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">待发货</label>';
            		case 3:
            			return '<label class="layui-btn-blue layui-btn-xs layui-btn" style="cursor:auto">待收货</label>';
            		case 4:
            			return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">已完成</label>';
            		case 5:
            			return '<label class="layui-btn-red layui-btn-xs layui-btn" style="cursor:auto">已失效</label>';
            		case 6:
            			return '<label class="layui-btn-red layui-btn-xs layui-btn" style="cursor:auto">已取消</label>';
            		case 7:
            			return '<label class="layui-btn-green layui-btn-xs layui-btn" style="cursor:auto">申请退款</label>';
            		case 8:
            			return '<label class="layui-btn-red layui-btn-xs layui-btn" style="cursor:auto">已退款</label>';
            	}
            	
            }},
            {field: 'createTime', title: '创建时间', align:'center',width:180},
            {title: '操作', templet:'#orderListBar',align:"center"}
        ]],
        done:function(){
        	//init();
        	if(curUser.userId != 1){
        		$(".del,.delAll_btn").remove();
        	}
        	
        	
        }
    });
    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".search_btn").on("click",function(){
       
        	tableIns.reload({
        		where: {
                    orderId: $(".searchVal").val()  //搜索的关键字
                }
        	})
        
    });

    //添加用户
    function addUser(edit){
        var index = layui.layer.open({
            title : "添加用户",
            type : 2,
            content : getUrl("userAdd.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    function viewOrder(data){
    	editId = data.orderNo;
    	var index = layui.layer.open({
            title : "订单详情",
            type : 2,
            area: ['900px', '500px'],
            content : getUrl("orderView.html"),
            success : function(layero, index){
            }
        })
    }
    
    
    function editOrder(data){
    	editId = data.orderNo;
        var index = layui.layer.open({
            title : "修改订单",
            type : 2,
            area: ['600px', '400px'],
            content : getUrl("orderEdit.html"),
            success : function(layero, index){
            }
        })
    //    layui.layer.full(index);
   //     window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
//        $(window).on("resize",function(){
//            layui.layer.full(window.sessionStorage.getItem("index"));
//        })
    }
    

    
    

    //列表操作
    table.on('tool(orderList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
        	editOrder(data);
        }else if(layEvent === 'view'){
        	viewOrder(data);
        }
        
        
    });

   
})
