layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
    	let userId = parent.editId;
    	ajaxRequest({
    		url:'/sys/user/info/'+userId,
    		type:'get',
    		success:function(data){
    			$(".userName").val(data.user.username);
    			$(".userEmail").val(data.user.email);
    			$(".userTel").val(data.user.mobile);
    			$("input[name='status'][value="+data.user.status+"]").attr("checked",true); 
    			let flag = false;
    			for(var i = 0; i<data.roles.length; i++){
    				for(var j=0;j<data.user.roleIdList.length;j++){
    					if(data.user.roleIdList[j] == data.roles[i].roleId){
    	    				$(".role").append('<input type="checkbox" checked name="roleIdList" value="'+data.roles[i].roleId+'" title="'+data.roles[i].roleName+'">');
    	    				flag = true;
    	    				break;
    					}
    				}
    				if(flag){
    					flag = false;
    					continue;
    				}
    				$(".role").append('<input type="checkbox" name="roleIdList" value="'+data.roles[i].roleId+'" title="'+data.roles[i].roleName+'">');
    			}
    			form.render();
    		}
    	})
    })
    
    
    
    form.on("submit(editUser)",function(data){
    	
    	     var arr = new Array();
    	      $("input:checkbox[name='roleIdList']:checked").each(function(i){
    	                arr[i] = $(this).val();
    	        });
    	data.field.roleIdList = arr;
    	data.field.userId = parent.editId;
        ajaxRequest({
        	url:'/sys/user/update',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("修改成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

   

})