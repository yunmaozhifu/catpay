layui.config({
	base : baseUrl +"/static/plugins/extends/"
}).extend({
	"authtree" : "authtree"
})

layui.use(['form','layer','authtree'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        authtree = layui.authtree;

    
    var config = {
		    	primaryKey: 'menuId'
		    ,startPid: 0
		    ,parentKey: 'parentId'
		    ,nameKey: 'name'
		    ,valueKey: 'menuId'
		    
		};
	   
    
	 if(parent.editId){
		 $(".roleName").toggleClass("layui-disabled");
		 $(".roleName").attr("disabled","disabled");
		 $(".roleUser").text("立即修改");
		 $(".reset").remove();
		 ajaxRequest({
			 url:'/sys/role/info/'+parent.editId,
			 type:'get',
			 async:false,
			 success:function(data){
				 $(".roleName").val(data.role.roleName);
				 $(".remark").val(data.role.remark);
				 config.checkedKey = data.role.menuIdList;
			 }
		 })
	 }
	
	 
	 ajaxRequest({
	    	url:'/sys/menu/list',
	    	success:function(data){
	    		var trees = authtree.listConvert(data, config);
	    		
	    		authtree.render('#LAY-auth-tree-index',trees, {
					inputname: 'authids[]'
					,layfilter: 'lay-check-auth'
					,autowidth: true
				});
	    	}
	    })
    
    
    form.on("submit(roleUser)",function(data){
    	let url = '/sys/role/save',msg='添加成功！';
    	if(parent.editId){
    		data.field.roleId = parent.editId;
    		url = '/sys/role/update';
    		msg ="修改成功！"
    	}
    	data.field.menuIdList = authtree.getChecked('#LAY-auth-tree-index');
        ajaxRequest({
        	url:url,
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg(msg);
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	}
        })
        return false;
    })

   

})