layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    
    form.on("submit(addConfig)",function(data){
        //弹出loading
        ajaxRequest({
        	url:'/sys/config/save',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("参数添加成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

   

})