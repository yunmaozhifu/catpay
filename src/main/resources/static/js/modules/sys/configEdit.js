layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
    	let id = parent.editId;
    	ajaxRequest({
    		url:'/sys/config/info/'+id,
    		type:'get',
    		success:function(data){
    			$(".paramKey").val(data.config.paramKey);
    			$(".paramValue").val(data.config.paramValue);
    			$(".remark").val(data.config.remark);
    			form.render();
    		}
    	})
    })
    
    
    
    form.on("submit(editConfig)",function(data){
    	
    	   
    	data.field.id = parent.editId;
        ajaxRequest({
        	url:'/sys/config/update',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("修改成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

   

})