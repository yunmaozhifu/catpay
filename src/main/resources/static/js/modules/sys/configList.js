layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var editId,curUser;
layui.use(['form','layer',"baseTable"],function(){
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laytpl = layui.laytpl,
		table = layui.baseTable;

	curUser =  $.parseJSON(sessionStorage.getItem("curUser"));



	//用户列表
	var tableIns = table.render({
		elem: '#configList',
		url : baseUrl+'/sys/config/list',
		cellMinWidth : 95,
		id : "configListTable",
		cols : [[
			{field: 'id', title: 'id',hide:true, minWidth:100, align:"center"},
			{field: 'paramKey', title: '参数名',width:250, align:"center"},
			{field: 'paramValue', title: '参数值', width:400, align:'center'},
			{field: 'remark', title: '备注', width:200, align:'center'},
			{title: '操作', templet:'#configListBar',align:"center"}
		]],
		done:function(){
			//init();
			if(curUser.userId != 1){
				//$(".del,.delAll_btn").remove();
			}


		}
	});
	//搜索【此功能需要后台配合，所以暂时没有动态效果演示】
	$(".search_btn").on("click",function(){
		tableIns.reload({
			where: {
				paramKey: $(".searchVal").val()  //搜索的关键字
			}
		})

	});

	$(".addConfig_btn").click(function(){
		addConfig();
	})

	//添加用户
	function addConfig(edit){
		var index = layui.layer.open({
			title : "添加参数",
			type : 2,
			content : getUrl("configAdd.html"),
			success : function(layero, index){
				setTimeout(function(){
					layui.layer.tips('点击此处返回参数列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},500)
			}
		})
		layui.layer.full(index);
		window.sessionStorage.setItem("index",index);
		//改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
		$(window).on("resize",function(){
			layui.layer.full(window.sessionStorage.getItem("index"));
		})
	}


	function editConfig(edit){
		editId = edit.id;
		var index = layui.layer.open({
			title : "编辑参数",
			type : 2,
			content : getUrl("configEdit.html"),
			success : function(layero, index){
				setTimeout(function(){
					layui.layer.tips('点击此处返回参数列表', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},500)
			}
		})
		layui.layer.full(index);
		window.sessionStorage.setItem("index",index);
		//改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
		$(window).on("resize",function(){
			layui.layer.full(window.sessionStorage.getItem("index"));
		})
	}





	//列表操作
	table.on('tool(configList)', function(obj){
		var layEvent = obj.event,
			data = obj.data;
		if(layEvent === 'edit'){ //编辑
			editConfig(data);
		}else if(layEvent === 'del'){
			layer.confirm('确定删除此参数？',{icon:3, title:'提示信息'},function(index){
				ajaxRequest({
					url:'/sys/config/delete',
					param:[data.id],
					success:function(){
						table.reload("configListTable");
						baseAlert("删除成功");
					}
				})
				layer.close(index);

			});
		}

	});


})
