layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    $(function(){
    	ajaxRequest({
    		url:'/sys/role/select',
    		success:function(data){
    			for(var i = 0; i<data.list.length; i++){
    				$(".role").append('<input type="checkbox" name="roleIdList" value="'+data.list[i].roleId+'" title="'+data.list[i].roleName+'">');
    			}
    			form.render();
    		}
    	})
    })
    
    form.verify({
    	Pwd:function(value,item){
    		if(value.length<6){
    			return "密码长度不能小于6位";
    		}
    	},
    	confirmPwd:function(value){
    		if(!new RegExp($(".passWord").val()).test(value)){
    			return "两次输入密码不一致，请重新输入！";
    		}
    	}
    })
    
    
    
    form.on("submit(addUser)",function(data){
        //弹出loading
    	
    	     var arr = new Array();
    	      $("input:checkbox[name='roleIdList']:checked").each(function(i){
    	                arr[i] = $(this).val();
    	        });
    	data.field.roleIdList = arr;
        ajaxRequest({
        	url:'/sys/user/save',
        	param:data.field,
        	needLoader:true,
        	success:function(){
        		 top.layer.msg("用户添加成功！");
                 layer.closeAll("iframe");
                 parent.location.reload();
        		
        	},
        	faile:function(data){
        		baseAlert(data.msg);
        	}
        })
        
        return false;
    })

   

})