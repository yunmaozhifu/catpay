layui.config({
	base : baseUrl +"/static/plugins/extends/"
}).extend({
	"treetable" : "treetable"
})

layui.use(['form','layer','treetable'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        treetable = layui.treetable;
    let result;
    ajaxRequest({
    	url:'/sys/menu/listAll',
    	async:false,
    	success:function(data){
    		result = data;
    	}
    })

    for(let i=0; i<result.length;i++){
    	result[i].id = result[i].menuId;
    	result[i].pid = result[i].parentId;
    	result[i].title = result[i].name;
    	result[i].icon = result[i].icon==null?'':result[i].icon;
    	result[i].list = result[i].list==null?'':result[i].list;
    	result[i].parentName = result[i].parentName==null?'':result[i].parentName;
    	result[i].perms = result[i].perms==null?'':result[i].perms;
    	result[i].url = result[i].url==null?'':result[i].url;
    	
    }
	/*var data=[{"id":1,"pid":0,"title":"1-1"},{"id":2,"pid":0,"title":"1-2"},{"id":3,"pid":0,"title":"1-3"},{"id":4,"pid":1,"title":"1-1-1"},{"id":5,"pid":1,"title":"1-1-2"},{"id":6,"pid":2,"title":"1-2-1"},{"id":7,"pid":2,"title":"1-2-3"},{"id":8,"pid":3,"title":"1-3-1"},{"id":9,"pid":3,"title":"1-3-2"},{"id":10,"pid":4,"title":"1-1-1-1"},{"id":11,"pid":4,"title":"1-1-1-2"}];
	treetable.render({
		elem: '#id-tree-table',
		data: data,
		field: 'title',
		is_checkbox: true,
		icon_val: {
			open: "&#xe619;",
			close: "&#xe61a;"
		},
		space: 4,
		cols: [
			{
				field: 'title',
				title: '标题',
				width: '30%',
			},
			{
				field: 'id',
				title: 'ID',
				width: '20%'
			},
			{
				title: '状态',
				width: '20%',
				template: function(item){
					return '<input type="checkbox" lay-skin="switch" lay-filter="status" lay-text="开启|关闭">';
				}
			},
			{
				field: 'pid',
				title: '父ID',
				width: '20%',
			},
			{
				field: 'actions',
				title: '操作',
				width: '30%',
				template: function(item){
					var tem = [];
					if(item.pid == 0){
						tem.push('<a class="add-child" lay-filter="add">添加子级</a>');
					}
					tem.push('<a lay-filter="edit">编辑</a>');
					if(item.pid > 0){
						tem.push('<a class="set-attr">设置属性</a>');
					}
					return tem.join(' <font>|</font> ')
				},
			},
		]
	});*/
	
    treetable.render({
		elem: '#id-tree-table',
		data:result,
		field: 'title',
		is_checkbox: false,
		/*icon_val: {
			open: "&#xe619;",
			close: "&#xe61a;"
		},*/
		space: 4,
		cols: [
			{
				field: 'title',
				title: '名称',
				width: '15%',
			},
			{
				field: 'parentName',
				title: '上级菜单',
				width: '10%'
			},
			{
				title: '图标',
				width: '5%',
				field: 'icon'
			},
			{
				title: '类型',
				width: '10%',
				template:function(item){
					switch (item.type) {
					case 0:
						return '<span class="dir">目录</span>';
					case 1:
						return '<span class="dir menu">菜单</span>';
					case 2:
						return '<span class="dir button">按钮</span>';
					default:
						break;
					}
				}
			},
			{
				field: 'orderNum',
				title: '排序',
				width: '5%',
			},
			{
				field: 'url',
				title: '菜单URL',
				width: '20%',
			},
			{
				field: 'perms',
				title: '授权标识',
				width: '20%',
			},
			{
				field: 'actions',
				title: '操作',
				width: '15%',
				template: function(item){
					console.log(item)
					var tem = [];
					tem.push('<a class="layui-btn layui-btn-xs" lay-filter="edit" >修改</a>');
					tem.push('<a class="layui-btn layui-btn-xs layui-btn-danger " lay-filter="del">删除</a>');
					return tem.join('')
				},
			},
		]
	});

	init();
    //添加用户
    function addMenu(){
        var index = layui.layer.open({
            title : "新增菜单",
            type : 2,
            content : getUrl("menuAdd.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    $(".addMenu_btn").click(function(){
    	addMenu();
    })

    
	treetable.on('treetable(edit)',function(data){
		layer.msg('修改操作');
		console.dir(data);
	})
	
	treetable.on('treetable(del)',function(data){
		 layer.confirm('确定删除此菜单？',{icon:3, title:'提示信息'},function(index){
         	ajaxRequest({
         		url:'/sys/menu/delete/'+data.item.menuId,
         		needLoader:true,
         		success:function(){
         			location.reload();
         			baseAlert("删除成功");
         		}
         	})
         	layer.close(index);
         	
         });
	})

})
