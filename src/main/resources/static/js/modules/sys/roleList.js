layui.config({
	base : baseUrl +"/static/js/common/"
}).extend({
	"baseTable" : "baseTable"
})
var editId,curUser;
layui.use(['form','layer',"baseTable"],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.baseTable;
    
         curUser =  $.parseJSON(sessionStorage.getItem("curUser"));
    //用户列表
    var tableIns = table.render({
        elem: '#roleList',
        url : baseUrl+'/sys/role/list',
        cellMinWidth : 95,
        id : "roleListTable",
        cols : [[
            {field:"1", type: "checkbox", width:50},
            {field: 'roleId', title: '角色ID',hide:true, minWidth:100, align:"center"},
            {field: 'roleName', title: '角色名称', minWidth:100, align:"center"},
            {field: 'remark', title: '备注', align:'center'},
            {field: 'createTime', title: '创建时间', minWidth:200, align:'center'},
            {title: '操作', minWidth:175, templet:'#roleListBar',align:"center"}
        ]]
    });
    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".search_btn").on("click",function(){
       
        	tableIns.reload({
        		where: {
        			roleName: $(".searchVal").val()  //搜索的关键字
                }
        	})
        
    });

    //添加用户
    function addRole(){
        var index = layui.layer.open({
            title : "新增角色",
            type : 2,
            content : getUrl("roleAddEdit.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    function editRole(edit){
    	editId = edit.roleId;
        var index = layui.layer.open({
            title : "编辑角色",
            type : 2,
            content : getUrl("roleAddEdit.html"),
            success : function(layero, index){
              setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    $(".addRoles_btn").click(function(){
    	addRole();
    })

   
    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('roleListTable'),
            data = checkStatus.data,
            roleIds = [];
        if(data.length > 0) {
            for (var i in data) {
            	roleIds.push(data[i].roleId);
            }
            layer.confirm('确定删除选中的角色？', {icon: 3, title: '提示信息'}, function (index) {
            	ajaxRequest({
            		url:'/sys/role/delete',
            		param:roleIds,
            		success:function(){
            			table.reload("roleListTable")
            			baseAlert("删除成功");
            		}
            	})
            	layer.close(index);
            	
            })
        }else{
            layer.msg("请选择需要删除的角色");
        }
    })

    //列表操作
    table.on('tool(roleList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
        	editRole(data);
        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此角色？',{icon:3, title:'提示信息'},function(index){
            	ajaxRequest({
            		url:'/sys/role/delete',
            		param:[data.roleId],
            		success:function(){
            			table.reload("roleListTable");
            			baseAlert("删除成功");
            		}
            	})
            	layer.close(index);
            	
            });
        }
    });

})
