layui.config({
	base : baseUrl +"/static/plugins/extends/"
}).extend({
	"treeSelect" : "treeSelect"
})


layui.use(['form','layer','treeSelect'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        treeSelect=layui.treeSelect;
    
	    form.on('radio()', function(data){
	    	switch (data.value) {
			case '0':
				$("#menu_select").addClass("layui-hide");
				$(".sort").removeClass("layui-hide");
				break;
			case '1':
				$("#menu_select").removeClass("layui-hide");
				$(".sort").removeClass("layui-hide");
				break;
			case '2':
				$("#menu_select").addClass("layui-hide");
				$(".sort").addClass("layui-hide");
				break;
			default:
				break;
			}
	    }); 

	    treeSelect.render({
            // 选择器
            elem: '#menu-tree',
            // 数据
            data: baseUrl+'/sys/menu/select',
            type: 'post',
            placeholder: '',
            search: true,
            click: function(d){
            	$(".parentId").val(d.current.id);
            	$(".parentName").val(d.current.name)
            },
            success: function (d) {
           
            	if(d.data.length != 0 ){
            		
            		$(".parentId").val(d.data[0].id);
                	$("#treeSelect-input-"+d.treeId.split('-')[2]).val(d.data[0].name);
                	$(".parentName").val(d.data[0].name);
            	}
            	

            }
        });
	    

	    form.on("submit(addMenu)",function(data){
	    	let url = '/sys/menu/save',msg='添加成功！';
	        ajaxRequest({
	        	url:url,
	        	param:data.field,
	        	needLoader:true,
	        	success:function(){
	        		 top.layer.msg(msg);
	                 layer.closeAll("iframe");
	                 parent.location.reload();
	        		
	        	}
	        })
	        return false;
	    })
   
})