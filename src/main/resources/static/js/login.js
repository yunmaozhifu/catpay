layui.use(['form','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer
        $ = layui.jquery;
    var uuid = 	"";
   $(function(){
	   changeImg();
   })
    
    function changeImg() {
	   	uuid = getUUID();
        var url =baseUrl+"/captcha.jpg?uuid="+uuid;
        $('#verifyCode').attr("src",url);
        $('#verifyCode').css("width","40%").css("height","38px");
    }
   
   $("#verifyCode").click(function(){
	   changeImg();
   })
  

    //登录按钮
    form.on("submit(login)",function(data){
    	var that = $(this);
    	that.text("登录中...").attr("disabled","disabled").addClass("layui-disabled");
        data.field.uuid = uuid;
       ajaxRequest({
        	url:'/sys/login',
        	param:data.field,
        	success:function(data){
        		sessionStorage.setItem("token", data.token); 
        		window.location.href = baseUrl+"/modules/index.html?token="+data.token;
        	},faile:function(data){
        		that.text("登录").removeAttr("disabled").removeClass("layui-disabled");
        		$("#captcha").val("");
        		$(".layui-input").blur();
        		if(data.msg.indexOf("验证码不能为空")!=-1){}
        		else
        			changeImg();
        		baseAlert(data.msg);
        	}
        });
        return false;
    })

    //表单输入效果
    $(".loginBody .input-item").click(function(e){
        e.stopPropagation();
        $(this).addClass("layui-input-focus").find(".layui-input").focus();
    })
    $(".loginBody .layui-form-item .layui-input").focus(function(){
        $(this).parent().addClass("layui-input-focus");
    })
    $(".loginBody .layui-form-item .layui-input").blur(function(){
        $(this).parent().removeClass("layui-input-focus");
        if($(this).val() != ''){
            $(this).parent().addClass("layui-input-active");
        }else{
            $(this).parent().removeClass("layui-input-active");
        }
    })
})
