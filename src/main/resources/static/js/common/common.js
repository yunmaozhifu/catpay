var baseUrl = getContextPath(); 
function getUUID () {
	  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
	    return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
	  })
}
 
function init(){
	
	var tips;
	$("td").on("mouseenter", function() {
        if (this.offsetWidth < this.scrollWidth) {
            var that = this;
            var text = $(this).text();
            tips = window.layer.tips(text, that, {
                tips: 1,
                time: -1
            });
        }
    });
	
	$("td").on("mouseleave", function() {
		layer.close(tips);
    });
}

 
 function getContextPath(){ 
		var pathName = document.location.pathname; 
		var index = pathName.substr(1).indexOf("/"); 
		var result = pathName.substr(0,index+1); 
		return result; 
}

//重写alert
function baseError(msg, callback){
 	parent.layer.alert(msg, {icon: 2},function(index){
 		parent.layer.close(index);
 		if(typeof(callback) === "function"){
 			callback("ok");
 		}
 	});
 }

function baseAlert(msg){
	parent.layer.msg(msg);
}

function cloneObject(src,proArr){
	if(proArr != null){
		var result = {};
		for(var key in src){
			if(proArr.indexOf(key) > -1){
				result[key] = src[key];
			}
		}
		return result;
	}else{
		var buf;
		if(src instanceof Array){
			buf = [];  // 创建一个空的数组
			var i = src.length;
			while (i--) {
				buf[i] = cloneObject(src[i],null);
			}
			return buf;
		}else if(src instanceof Object){
			buf={};
			for (var k in src) {  // 为这个对象添加新的属性
				buf[k] = cloneObject(src[k],null);
			}
			return buf
		}else{
			return src;
		} 
	
	}
	
}

function getUrl(url){
	return url +"?token="+sessionStorage.getItem("token");
}

$.ajaxSetup({
	headers:{
		token:sessionStorage.getItem("token")==null?'':sessionStorage.getItem("token")
	}

})
 
 function ajaxRequest(obj){
		//设置请求超时时间,默认为：30 * 10000
	    var timeout=30 * 10000; 
	    if(obj.timeout){timeout=obj.timeout;}
	    //设置请求方式，默认为POST
	    var type="POST";
	    if(obj.type){type=obj.type;}
	    //设置信息至服务器时内容编码类型，默认为application/json
	    var contentType = "application/json";
	    if(obj.contentType){contentType = obj.contentType;}   
	    //设置请求参数 
	    var data = obj.param;
	    if(contentType == "application/json"){ data = JSON.stringify(obj.param);}
	    //设置是否异步，默认为true，异步请求
	    var async = true;
	    if(obj.async === false){async = false;}
	    //设置是否在url后加timestamp
	    var url = baseUrl+obj.url+"?timestamp="+new Date().getTime();
	    if(obj.timestamp === false){url = baseUrl+obj.url;}
	    
	    
	    var index = null;
	    
	    var options = {		  
				contentType:contentType,
			    dataType : "json",
	            traditional:true,
	            cache:false,
	            timeout: timeout,
	            type: type,  
	            url: url,
	            data: data, 
	            async: async,
	            beforeSend:function(){
	            	//如果needLoader为false，不需要loading框;
		        	if(obj.needLoader==true){index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});}
		        },
		        complete:function(){
		        	if(obj.needLoader==true){
		        		top.layer.close(index);
		        	}
		        },
	            success:function(data){
	            	
	            	if(data.status && obj.success){
	            		obj.success(data.result);
	            	}else if(!data.status && data.code === 500){
	            		baseError(data.msg);
	            	}else if(!data.status && data.code === 302){
	            		window.location.href = baseUrl+"/login.html";
	            	}else if (!data.status && obj.faile) {  
	            		if(data.msg)
	            			obj.faile({'code': data.code,'msg': data.msg});
	            		else if(data.message)
	            			obj.faile({'code': data.code,'msg': data.message});
	                } else if (!data.status) {
	                	if(data.msg)
	                		baseAlert(data.msg);
	                	else if(data.message)
	                		baseAlert(data.message);
	                }
	            },
	            error: function(e, xhr, type){
	                if(obj.error) {
	                	obj.error(e, xhr, type);
	                }else{
	                	baseError("网络异常，请稍后重试");
	                }
	            }
	    };
	    
	    $.ajax(options);
 }
