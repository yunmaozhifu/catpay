layui.define(['table','laytpl'],function(exports){
	var $ = layui.jquery,
    	laytpl = layui.laytpl,
    	laypage = layui.laypage,
    	table = layui.table;

	table.set({
		headers:{
			token:sessionStorage.getItem("token"),
			
		},
		contentType:'application/json;charset=UTF-8',
		method:'post',
		 text: {
			    none: '暂无相关数据' 
		 },
		 size:'l',
		 height : "full",
		 loading:false,
		page:{
		    layout: [ 'count','prev', 'page', 'next'] //自定义分页布局
	      ,curr: 1 
	    //  ,first: '首页' //不显示首页
	     // ,last: '尾页'//不显示尾页
	      ,prev: '上一页'
	      ,next: '下一页'
	      ,theme: '#1E9FFF'
		}
		 
		,parseData: function(res){ //res 即为原始返回的数据
		    return {
		      "code": res.code, //解析接口状态
		      "msg": res.msg, //解析提示文本
		      "count": res.result.page.totalCount, //解析数据长度
		      "data": res.result.page.list //解析数据列表
		    };
		  },
		  response: {
			  statusCode: 200
		  }
	});
	
	exports("baseTable",table);
});