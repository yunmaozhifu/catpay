@echo off
rem ======================================================================
rem windows startup script
rem
rem author: wgy
rem date: 2019-4-23
rem ======================================================================

rem startup jar
java -Dloader.path="../lib/,../config/" -jar ../boot/@project.artifactId@-@project.version@.jar --spring.config.location=../config/

pause