/*
Navicat MySQL Data Transfer

Source Server         : 39.108.186.240
Source Server Version : 50726
Source Host           : 39.108.186.240:3306
Source Database       : yunmao

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-20 12:04:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ape_hot_msg`
-- ----------------------------
DROP TABLE IF EXISTS `ape_hot_msg`;
CREATE TABLE `ape_hot_msg` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户id',
  `type` int(2) DEFAULT '0' COMMENT '消息类型。1：系统通知；2：用户通知',
  `title` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '标题',
  `context` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '消息内容',
  `link` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '推送url路径',
  `is_read` int(2) DEFAULT '0' COMMENT '是否已读。1：是；0：否',
  `sort_num` int(11) DEFAULT '0' COMMENT '越大越前',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ape_hot_msg
-- ----------------------------
INSERT INTO `ape_hot_msg` VALUES ('1', '3', '0', '测试', '', 'https://www.baidu.com/', '0', '0', '2019-08-14 20:43:21');

-- ----------------------------
-- Table structure for `ape_income_records`
-- ----------------------------
DROP TABLE IF EXISTS `ape_income_records`;
CREATE TABLE `ape_income_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单号',
  `phone` varchar(15) NOT NULL COMMENT '手机号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `rule` varchar(50) DEFAULT NULL COMMENT '规则',
  `remark` varchar(255) DEFAULT NULL COMMENT '规则',
  `buy_user_id` bigint(20) DEFAULT NULL COMMENT '交易用户',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '订单金额',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `type` int(11) DEFAULT '0' COMMENT '收益类型。1：激活pos机；2：刷卡；4：发展下级',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `income_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='收益';

-- ----------------------------
-- Records of ape_income_records
-- ----------------------------
INSERT INTO `ape_income_records` VALUES ('1', '12345', '15915840210', '3', '123', '1', '123', '123', '10.00', '2019-08-14 21:07:13', '0');
INSERT INTO `ape_income_records` VALUES ('2', '321456', '15915840211', '3', '1234', '1', '134', '123', '30.00', '2019-08-14 21:22:18', '0');
INSERT INTO `ape_income_records` VALUES ('3', '18927564131', '18927564131', '4', '18927564131', '18927564131', '18927564131', '3', '23.44', '2019-08-16 22:13:43', '1');
INSERT INTO `ape_income_records` VALUES ('4', '18927564131', '18927564131', '4', '18927564131', '18927564131', '18927564131', '1', '12.30', '2019-08-14 21:07:13', '1');

-- ----------------------------
-- Table structure for `ape_income_rule`
-- ----------------------------
DROP TABLE IF EXISTS `ape_income_rule`;
CREATE TABLE `ape_income_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `goods_id` bigint(20) NOT NULL COMMENT '用户id',
  `cond` int(11) DEFAULT '0' COMMENT '数量',
  `user_chose` int(11) DEFAULT '2' COMMENT '// 奖励给谁 1本人2推荐人3推荐人的推荐人',
  `to_pather_role` int(11) DEFAULT '0' COMMENT '角色',
  `to_pather_level` int(11) DEFAULT '0' COMMENT '级别',
  `aumt` varchar(20) DEFAULT NULL COMMENT '金额/百分比',
  `status` int(11) DEFAULT '0' COMMENT '-1停用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `goods_id_index` (`goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='收货地址';

-- ----------------------------
-- Records of ape_income_rule
-- ----------------------------
INSERT INTO `ape_income_rule` VALUES ('1', '1', '1', '2', '1', '1', '100', '-1', '2019-08-16 14:41:13');
INSERT INTO `ape_income_rule` VALUES ('2', '3', '1', '2', '1', '1', '200', '-1', '2019-08-16 15:27:37');
INSERT INTO `ape_income_rule` VALUES ('3', '1', '1', '2', '1', '3', '999', '-1', null);

-- ----------------------------
-- Table structure for `ape_order_records`
-- ----------------------------
DROP TABLE IF EXISTS `ape_order_records`;
CREATE TABLE `ape_order_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单号',
  `transfer_no` varchar(100) DEFAULT NULL COMMENT '交易号对应支付系统',
  `phone` varchar(15) NOT NULL COMMENT '手机号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `goods_name` varchar(20) DEFAULT NULL COMMENT '商品名',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '订单金额',
  `total_count` int(11) DEFAULT '0' COMMENT '数量',
  `goods_price` decimal(10,2) DEFAULT '0.00' COMMENT '单价',
  `goods_type` int(11) DEFAULT '0' COMMENT '1虚拟2实物',
  `address_id` bigint(20) DEFAULT NULL COMMENT '地址ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(11) DEFAULT '0' COMMENT '订单状态。1：待付款；2：待发货；3：待收货；4：已完成；5：已失效；6：已取消；7：申请退款；8：已退款',
  `image` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `order_no_index` (`order_no`) USING BTREE,
  KEY `user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单';

-- ----------------------------
-- Records of ape_order_records
-- ----------------------------
INSERT INTO `ape_order_records` VALUES ('1', '123456', '123', '123', '1', '55', '5', '5', '34.00', '22', '0.00', '0', '1', '2019-08-14 20:07:35', '3', null);
INSERT INTO `ape_order_records` VALUES ('2', 'O_f47cf622bf4642e5b744b20e0bcd3226', null, '15915840210', '3', '岁岁', '1', '测试', '2400.00', '200', '12.00', '1', '123', null, '6', null);
INSERT INTO `ape_order_records` VALUES ('3', 'O_cc28a530f9784f83ab7a6cec8b530a7e', null, '18122200630', '2', '123', '1', '初级供应商', '100.00', '1', '100.00', '1', '18122200630', null, '1', null);
INSERT INTO `ape_order_records` VALUES ('4', 'O_34e5deb843b745bdb31d0a75d864c8cd', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('5', 'O_b748025af9084e0ea841d3ed8ace6ba4', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '6', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('6', 'O_014ab90d2da94bed8e727514dab750a2', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '6', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('7', 'O_6392b24b294b4f0b8650908f34ad0379', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '6', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('8', 'O_4c3486edf87f43a196e4ab72f2754fd6', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('9', 'O_d885a4a881e84932b430c1acf52cbe1d', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '3', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('10', 'O_317e013732224497ae0e49b3426b26bd', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '5', '100.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('11', 'O_6440111201c9489d9ab71fb5cf5c80a1', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('12', 'O_356731177ed54d6ea454f7de03c091b0', null, '18927564131', '4', '测试', '6', '测试上传图片', '100.00', '1', '100.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('13', 'O_3f5c227ebefb4c41aa5bf0e8ea4e2abd', null, '18927564131', '4', '测试', '8', '测试详情', '10.00', '1', '10.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('14', 'O_2a67e437e8fe4491b359127155c6373f', null, '18927564131', '4', '测试', '6', '测试上传图片', '1000.00', '1', '1000.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('15', 'O_19aa6f37c2224793a0eac1951a7ccb74', null, '18927564131', '4', '测试', '6', '测试上传图片', '1000.00', '1', '1000.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('16', 'O_4f583fd2f8004bdd88ee39db6a55e3d9', null, '18927564131', '4', '测试', '102', '测试', '496.00', '4', '124.00', '1', '123123', null, '1', 'http://img3.imgtn.bdimg.com/it/u=2467391851,2018402976&fm=26&gp=0.jpg');
INSERT INTO `ape_order_records` VALUES ('17', 'O_08e4356256f64ced8b127f12639d7cdc', null, '18927564131', '4', '测试', '102', '测试', '496.00', '4', '124.00', '1', '123123', null, '1', 'http://img3.imgtn.bdimg.com/it/u=2467391851,2018402976&fm=26&gp=0.jpg');
INSERT INTO `ape_order_records` VALUES ('18', 'O_3fa72337b1004808a7a3347e15ff76f4', null, '18927564131', '4', '测试', '6', '测试上传图片', '0.00', '1', '0.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('19', 'O_4587d4034889457f8b9f2f58cd51a0a0', null, '18927564131', '4', '测试', '6', '测试上传图片', '0.00', '1', '0.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('20', 'O_d263df6208e341daa1c88a9928b26b87', null, '18927564131', '4', '测试', '102', '测试', '124.00', '1', '124.00', '1', '123123', null, '1', 'http://img3.imgtn.bdimg.com/it/u=2467391851,2018402976&fm=26&gp=0.jpg');
INSERT INTO `ape_order_records` VALUES ('21', 'O_9b8c2a9f58bd481b86fc5584daa44945', null, '18927564131', '4', '测试', '6', '测试上传图片', '1.00', '1', '1.00', '1', '123123', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('22', 'O_04b214ed9ca34861b7da4e0ad45d92bb', null, '17671617909', '5', '啊蒙', '6', '测试上传图片', '1.00', '1', '1.00', '1', '4', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('23', 'O_936907d4c45241e98b181ab5a2501033', null, '17671617909', '5', '啊蒙', '6', '测试上传图片', '1.00', '1', '1.00', '1', '4', null, '1', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('24', 'O_c89ca73b81ce4bda8de8a165acbe683d', null, '17671617909', '5', '啊蒙', '6', '测试上传图片', '1.00', '1', '1.00', '1', '4', null, '4', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('25', 'O_b30527cf051e415cb9b97eaa397e6d85', null, '17671617909', '5', '啊蒙', '6', '测试上传图片', '1.00', '1', '1.00', '1', '4', null, '4', 'http://image.yidongtuozhan.com/pro_063b87feef974f478dbe07d25455fdb8.JPG');
INSERT INTO `ape_order_records` VALUES ('26', 'O_f65ecefd01b740d585d27d57e3d0575f', null, '17671617909', '12', '阿三', '1', '初级供应商', '100.00', '1', '100.00', '1', '5', null, '1', null);
INSERT INTO `ape_order_records` VALUES ('27', 'O_940bdf1f1cd441e2816345ef70d73d4d', null, '18927564131', '4', '测试', '2', '中级供应商', '10000.00', '1', '10000.00', '1', '2', null, '1', 'http://image.yidongtuozhan.com/pro_3ba13c0c438c43c8a3b0bec77122cfa4.jpg');
INSERT INTO `ape_order_records` VALUES ('28', 'O_2b6cfcd17acd45cda793d0069384c893', null, '13533846734', '17', 'hoo', '2', '中级供应商', '10000.00', '1', '10000.00', '0', '6', null, '1', 'http://image.yidongtuozhan.com/pro_3ba13c0c438c43c8a3b0bec77122cfa4.jpg');
INSERT INTO `ape_order_records` VALUES ('29', 'O_83d52297063f431a8673bd28f4b01878', null, '18122200630', '18', '王文', '101', '中级供应商', '19999.00', '1', '19999.00', '1', '7', null, '1', 'http://image.yidongtuozhan.com/pro_ef1127fbf1e94227af8f3880540e56c5.jpg');
INSERT INTO `ape_order_records` VALUES ('30', 'O_76f0819f4e8b4aa18a467854cde29b1e', null, '13533846734', '17', 'hoo', '3', '高级供应商', '30000.00', '1', '30000.00', '0', '6', null, '6', 'http://image.yidongtuozhan.com/pro_f96ea16dda4a40db8f4d8dbd46cbe2d1.jpg');

-- ----------------------------
-- Table structure for `ape_product_info`
-- ----------------------------
DROP TABLE IF EXISTS `ape_product_info`;
CREATE TABLE `ape_product_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `subtitle` varchar(128) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `pro_order` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `imgUrl` varchar(512) DEFAULT NULL,
  `createTime` datetime NOT NULL,
  `isBannerProduct` tinyint(1) NOT NULL,
  `isGrounding` tinyint(1) NOT NULL,
  `details` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ape_product_info
-- ----------------------------
INSERT INTO `ape_product_info` VALUES ('1', '初级供应商', '初级供应商', '1999', '2', '0', 'http://image.yidongtuozhan.com/pro_ce393c91a4184b7285a4dfa1ffc4b377.jpg', '2019-08-15 14:09:40', '1', '1', '这是一个初级会员');
INSERT INTO `ape_product_info` VALUES ('2', '中级供应商', '中级供应商', '10000', '1', '0', 'http://image.yidongtuozhan.com/pro_3ba13c0c438c43c8a3b0bec77122cfa4.jpg', '2019-08-15 14:10:46', '1', '1', '这是一个中级会员');
INSERT INTO `ape_product_info` VALUES ('3', '高级供应商', '高级供应商', '30000', '0', '0', 'http://image.yidongtuozhan.com/pro_f96ea16dda4a40db8f4d8dbd46cbe2d1.jpg', '2019-08-15 14:11:24', '1', '1', '这是一个高级会员');
INSERT INTO `ape_product_info` VALUES ('6', '中级供应商', '中级供应商', '19999', '123', '1', 'http://image.yidongtuozhan.com/pro_d684c8a6be5a477285f74b2efc647a55.jpg', '2019-08-15 14:30:19', '0', '1', '官方产品');
INSERT INTO `ape_product_info` VALUES ('7', '初级供应商', '初级供应商', '1999', '0', '1', 'http://image.yidongtuozhan.com/pro_4f29514853624680b3ed323b8156f89f.jpg', '2019-08-15 22:09:08', '0', '0', '888');
INSERT INTO `ape_product_info` VALUES ('8', '高级供应商', '高级供应商', '30000', '20', '1', 'http://image.yidongtuozhan.com/pro_84ef9215bd7a4ea4a4d9dd4f7ce69128.jpg', '2019-08-15 22:12:47', '0', '1', '这是一个高级会员');
INSERT INTO `ape_product_info` VALUES ('101', '中级供应商', '中级供应商', '19999', '0', '1', 'http://image.yidongtuozhan.com/pro_ef1127fbf1e94227af8f3880540e56c5.jpg', '2019-08-13 21:17:14', '0', '1', '官方产品');
INSERT INTO `ape_product_info` VALUES ('102', '初级会员', '初级会员', '1999', '1999', '1', 'http://image.yidongtuozhan.com/pro_ca0fbfc034694e24b1124d352fe78bf2.jpg', '2019-08-13 21:47:54', '0', '1', '这是一个初级会员');
INSERT INTO `ape_product_info` VALUES ('103', '上传图片', '上传图片', '12', '0', '1', null, '2019-08-17 12:08:23', '0', '0', 'ddd');

-- ----------------------------
-- Table structure for `ape_send_records`
-- ----------------------------
DROP TABLE IF EXISTS `ape_send_records`;
CREATE TABLE `ape_send_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `send_no` varchar(50) DEFAULT NULL COMMENT '配送单号',
  `count` int(11) DEFAULT NULL COMMENT '配送数量',
  `state` varchar(10) DEFAULT '1' COMMENT '状态。1：已提交；2：已发货；3：已完成',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `express_no` varchar(50) DEFAULT NULL COMMENT '快递单号',
  `express_company` varchar(20) DEFAULT NULL COMMENT '快递公司',
  `name` varchar(20) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `province` varchar(20) DEFAULT NULL COMMENT '省份',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  `area` varchar(20) DEFAULT NULL COMMENT '区',
  `address` varchar(50) DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='配送记录';

-- ----------------------------
-- Records of ape_send_records
-- ----------------------------

-- ----------------------------
-- Table structure for `ape_share_image`
-- ----------------------------
DROP TABLE IF EXISTS `ape_share_image`;
CREATE TABLE `ape_share_image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `img` varchar(200) NOT NULL COMMENT '主图片',
  `order_no` int(11) DEFAULT NULL COMMENT '排序值',
  `comment` varchar(100) DEFAULT NULL COMMENT '备注',
  `user_id` bigint(20) DEFAULT NULL COMMENT '父ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='分享图片';

-- ----------------------------
-- Records of ape_share_image
-- ----------------------------
INSERT INTO `ape_share_image` VALUES ('6', 'http://image.yidongtuozhan.com/share_18122200630.jpg', null, '', '2');
INSERT INTO `ape_share_image` VALUES ('7', 'http://image.yidongtuozhan.com/share_18122200630.jpg', null, '', '2');
INSERT INTO `ape_share_image` VALUES ('11', 'http://image.yidongtuozhan.com/share_e47c88c1-1e5c-40dd-b437-f12ac0ae813e.jpg', null, null, '4');
INSERT INTO `ape_share_image` VALUES ('12', 'http://image.yidongtuozhan.com/share_f13bc87a-32ae-4ad2-a4e6-8ef481f4919d.jpg', null, null, '4');
INSERT INTO `ape_share_image` VALUES ('13', 'http://image.yidongtuozhan.com/share_d5a9ee6e-95cd-4edd-ab41-fb26c70c4c74.jpg', null, null, '4');
INSERT INTO `ape_share_image` VALUES ('14', 'http://image.yidongtuozhan.com/share_219489d3-caf5-437a-b266-895c67c6c30b.jpg', null, null, '5');
INSERT INTO `ape_share_image` VALUES ('15', 'http://image.yidongtuozhan.com/share_bfc1c595-e27e-4270-b773-c4d7f24c6e5d.jpg', null, null, '13');
INSERT INTO `ape_share_image` VALUES ('16', 'http://image.yidongtuozhan.com/share_89c0b46e-b1c4-4b9e-b4c3-026962aed38b.jpg', null, null, '14');
INSERT INTO `ape_share_image` VALUES ('17', 'http://image.yidongtuozhan.com/share_90f893b7-c696-4091-aa89-8f8c15dccb7c.jpg', null, null, '14');
INSERT INTO `ape_share_image` VALUES ('18', 'http://image.yidongtuozhan.com/share_257f1675-9527-4930-8cea-b5bda66a5135.jpg', null, null, '13');
INSERT INTO `ape_share_image` VALUES ('19', 'http://image.yidongtuozhan.com/share_63c3e7d4-deb5-4a29-b6db-bf325fcb47a3.jpg', null, null, '13');
INSERT INTO `ape_share_image` VALUES ('20', 'http://image.yidongtuozhan.com/share_69d5b9af-646b-4029-8ba3-01292e732919.jpg', null, null, '13');
INSERT INTO `ape_share_image` VALUES ('21', 'http://image.yidongtuozhan.com/share_4544f65c-43c8-41a3-9472-a2b7a936c76d.jpg', null, null, '13');
INSERT INTO `ape_share_image` VALUES ('22', 'http://image.yidongtuozhan.com/share_0a8a1cfa-36d6-4fc9-aaff-176244c50ef7.jpg', null, null, '13');
INSERT INTO `ape_share_image` VALUES ('23', 'http://image.yidongtuozhan.com/share_c93cb9e1-7516-4d05-9c0d-d0f01dd9c2f5.jpg', null, null, '12');
INSERT INTO `ape_share_image` VALUES ('24', 'http://image.yidongtuozhan.com/share_6023c370-d8af-4d8e-9dd1-39019b330f64.jpg', null, null, '12');
INSERT INTO `ape_share_image` VALUES ('25', 'http://image.yidongtuozhan.com/share_9076727b-161e-454f-97b4-62a74f99bc79.jpg', null, null, '12');
INSERT INTO `ape_share_image` VALUES ('26', 'http://image.yidongtuozhan.com/share_b6fda314-dbdd-47fd-9137-fe9ce542a873.jpg', null, null, '12');
INSERT INTO `ape_share_image` VALUES ('27', 'http://image.yidongtuozhan.com/share_9fbbdd7d-39da-4d00-b7d9-3904cf5bccc1.jpg', null, null, '12');
INSERT INTO `ape_share_image` VALUES ('28', 'http://image.yidongtuozhan.com/share_b51ee186-d0bc-4dc3-aa3e-f015e99d03bd.jpg', null, null, '12');
INSERT INTO `ape_share_image` VALUES ('29', 'http://image.yidongtuozhan.com/share_aaeb5a62-5247-47b5-89ac-da64f1074694.jpg', null, null, '15');
INSERT INTO `ape_share_image` VALUES ('36', 'http://image.yidongtuozhan.com/pro_c3f8daad4b2040629d5e7bdb4b1cddba.jpeg', null, null, '-2');
INSERT INTO `ape_share_image` VALUES ('37', 'http://image.yidongtuozhan.com/pro_3f90c8ad9ba5435f8a5f1f4bcf3da70b.jpeg', null, null, '-2');
INSERT INTO `ape_share_image` VALUES ('38', 'http://image.yidongtuozhan.com/pro_c7a490ce7ee2491d8dcd786f7f6341e5.jpeg', null, null, '-2');
INSERT INTO `ape_share_image` VALUES ('39', 'http://image.yidongtuozhan.com/pro_7f3b70bb162c4205931948ce4ca00f0a.jpeg', null, null, '-2');
INSERT INTO `ape_share_image` VALUES ('40', 'http://image.yidongtuozhan.com/pro_ace592f5b5c843cf9bffe46f6fb13fe9.jpeg', null, null, '-2');
INSERT INTO `ape_share_image` VALUES ('41', 'http://image.yidongtuozhan.com/pro_9c21747e3ef847b3bd6e6c9aa94f6f3c.jpeg', null, null, '-2');
INSERT INTO `ape_share_image` VALUES ('42', 'http://image.yidongtuozhan.com/pro_b4b0bcfc3ce640b788ef49daa625fd53.jpeg', null, null, '-1');
INSERT INTO `ape_share_image` VALUES ('43', 'http://image.yidongtuozhan.com/pro_fa35f3e184d74303876c184cb6e00f01.jpeg', null, null, '-1');
INSERT INTO `ape_share_image` VALUES ('44', 'http://image.yidongtuozhan.com/pro_212f6b20414249e7a3c6e89097d19db7.jpeg', null, null, '-1');
INSERT INTO `ape_share_image` VALUES ('45', 'http://image.yidongtuozhan.com/pro_6d4c5668672c42c981a1e582a4ec4c6a.jpeg', null, null, '-1');
INSERT INTO `ape_share_image` VALUES ('46', 'http://image.yidongtuozhan.com/pro_243a07d6f4074e9d8b2e719226a84e6b.jpeg', null, null, '-1');

-- ----------------------------
-- Table structure for `ape_user_address`
-- ----------------------------
DROP TABLE IF EXISTS `ape_user_address`;
CREATE TABLE `ape_user_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `name` varchar(20) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `province` varchar(20) DEFAULT NULL COMMENT '省份',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  `area` varchar(20) DEFAULT NULL COMMENT '区',
  `address` varchar(500) DEFAULT NULL COMMENT '详细地址',
  `is_default` char(1) DEFAULT '0' COMMENT '是否默认。1：是；0：否',
  `label` varchar(10) DEFAULT NULL COMMENT '标签',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='收货地址';

-- ----------------------------
-- Records of ape_user_address
-- ----------------------------
INSERT INTO `ape_user_address` VALUES ('1', '3', '岁岁3', '15915840211', '广东省1', '广州市1', '天河区1', '广东省广州市天河区1', '1', null, '2019-08-14 19:11:27');
INSERT INTO `ape_user_address` VALUES ('2', '4', '123', '213123', '北京市', '市辖区', '通州区', '嗯嗯呃', '1', null, '2019-08-15 15:48:53');
INSERT INTO `ape_user_address` VALUES ('3', '2', '123456', '18122200630', '3194', '3333', '2222', '123', '1', null, '2019-08-15 16:35:10');
INSERT INTO `ape_user_address` VALUES ('4', '5', '阿梦', '17671617909', '北京市', '市辖区', '西城区', '垃嗯嗯额懂', '1', null, '2019-08-17 00:52:08');
INSERT INTO `ape_user_address` VALUES ('5', '12', '哈哈哈哈', '哈哈哈哈', '辽宁省', '沈阳市', '和平区', '不实话实说', '0', null, '2019-08-17 06:19:27');
INSERT INTO `ape_user_address` VALUES ('6', '17', '胡生', '13533846734', '广东省', '广州市', '白云区', '急急急急急急看', '1', null, '2019-08-17 15:14:32');
INSERT INTO `ape_user_address` VALUES ('7', '18', '123', '18122300630', '北京市', '市辖区', '西城区', '哈哈哈哈', '1', null, '2019-08-17 17:19:44');

-- ----------------------------
-- Table structure for `ape_user_info`
-- ----------------------------
DROP TABLE IF EXISTS `ape_user_info`;
CREATE TABLE `ape_user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `phone` varchar(15) NOT NULL COMMENT '手机号',
  `recommon_number` varchar(50) DEFAULT NULL COMMENT '推荐码',
  `father_recommon_number` varchar(50) DEFAULT NULL COMMENT '上级推荐码',
  `father_id` bigint(20) DEFAULT NULL COMMENT '推荐人ID',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `head_image` varchar(255) DEFAULT '默认头像（待补充）' COMMENT '头像',
  `role` int(11) DEFAULT '0' COMMENT '0普通用户1供应商',
  `Level` int(11) DEFAULT '0' COMMENT '级别',
  `account_balance` decimal(10,2) DEFAULT '0.00' COMMENT '账户余额',
  `profit_balance` decimal(10,2) DEFAULT '0.00' COMMENT '收益总余额',
  `frozen_balance` decimal(10,2) DEFAULT '0.00' COMMENT '冻结金额',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(11) DEFAULT '0' COMMENT '状态-1冻结',
  `password` varchar(128) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `phone_index` (`phone`) USING BTREE,
  KEY `father_id_index` (`father_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户';

-- ----------------------------
-- Records of ape_user_info
-- ----------------------------
INSERT INTO `ape_user_info` VALUES ('1', '12345678911', '123456', '131213', '123', '测试', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-14 09:13:02', '0', '45456464');
INSERT INTO `ape_user_info` VALUES ('2', '18122200631', '18122200631', null, null, '123', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '1', '1', '0.00', '0.00', '0.00', '2019-08-14 16:20:16', '0', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92');
INSERT INTO `ape_user_info` VALUES ('3', '15915840210', '15915840210', null, null, '岁岁', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '500.00', '0.00', '50.00', '2019-08-14 18:36:48', '0', '15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225');
INSERT INTO `ape_user_info` VALUES ('4', '18927564131', '18927564131', '18122200631', '2', '测试', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-15 10:59:59', '0', 'cdf4a007e2b02a0c49fc9b7ccfbb8a10c644f635e1765dcf2a7ab794ddc7edac');
INSERT INTO `ape_user_info` VALUES ('12', '17671617909', '17671617909', '18927564131', '4', '阿三', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 04:23:49', '0', '02d09efdf0ea886531e7c9cef128e7b04d9761b60bd17fc94dbfcb0f2b2cc1e9');
INSERT INTO `ape_user_info` VALUES ('13', '13119598785', '13119598785', '17671617909', '12', '陈旭兴', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 04:46:33', '0', 'b066b90928f4bcac247a519d3173aaa010a4dfb5b5a71c115defb97abe13dd76');
INSERT INTO `ape_user_info` VALUES ('14', '18515675576', '18515675576', '13119598785', '13', 'maybe', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 05:27:40', '0', '101a57f8d7efd05310997baf73fdbbf55a17b4fbd00136fa6b92e6af6e82937a');
INSERT INTO `ape_user_info` VALUES ('15', '13660600072', '13660600072', '13119598785', '13', '肖科', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 09:15:56', '0', 'ddf92770f8b3fa8e52584e3801a4de9baf06edd87302b9d872ca20b5acfa7b15');
INSERT INTO `ape_user_info` VALUES ('17', '13533846734', '13533846734', '18122200630', '18', 'hoo', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 14:21:10', '0', '29e48aa328da6fb5235d6abb67a9a4ac008e805a732b40392d0fb2dd633becfc');
INSERT INTO `ape_user_info` VALUES ('18', '18122200630', '18122200630', '18927564131', '4', '王文', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 14:24:34', '0', 'cdf4a007e2b02a0c49fc9b7ccfbb8a10c644f635e1765dcf2a7ab794ddc7edac');
INSERT INTO `ape_user_info` VALUES ('19', '18520705028', '18520705028', '13119598785', '13', 'Andy', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 20:53:25', '0', '4bd2ca075ed87dbca48c763c16b375628e6148ff908c15fe3d05075482862be2');
INSERT INTO `ape_user_info` VALUES ('20', '13229479631', '13229479631', '18520705028', '19', '邓老师', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 21:29:10', '0', '8aa0f915485578cc4754dcd7ea0762e4ae1af1c88757f3565b97a77b38d2a55b');
INSERT INTO `ape_user_info` VALUES ('21', '13711765024', '13711765024', '18520705028', '19', '李耀杰', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 21:30:04', '0', '6b964a5326f2b798afe57ce923c6372d4c5a78ae9f614c698a8a0bb04f86d6cc');
INSERT INTO `ape_user_info` VALUES ('22', '13926456855', '13926456855', '18520705028', '19', '刘远鑫', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 21:30:21', '0', '9a68622f963f32cf31ebffbf08be886f31aebba805c475fd396c17160719cffa');
INSERT INTO `ape_user_info` VALUES ('23', '15219243886', '15219243886', '18520705028', '19', '郑巧巧', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 21:37:05', '0', 'cbbe55f727246d2fe5af59efec85299ba96cf2db0e8a9264dacffad68db3a6dd');
INSERT INTO `ape_user_info` VALUES ('24', '15200860569', '15200860569', '18520705028', '19', '曾海强', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-17 23:40:18', '0', 'd014b944ffa0c83b882048e9e5106b954d6e64ed74ef112b50e1aa2a98b56a1c');
INSERT INTO `ape_user_info` VALUES ('25', '13719207446', '13719207446', '13229479631', '20', '陈丽泉', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-18 13:54:42', '0', 'b8e6834737812886a878c76acafb549e8b44bd5e296612941e67b1c0f599031e');
INSERT INTO `ape_user_info` VALUES ('26', '13560212288', '13560212288', '13719207446', '25', '徐路旭', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-18 15:52:11', '0', 'cdf4a007e2b02a0c49fc9b7ccfbb8a10c644f635e1765dcf2a7ab794ddc7edac');
INSERT INTO `ape_user_info` VALUES ('27', '13533618837', '13533618837', '13560212288', '26', '邝锋', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-18 18:14:49', '0', 'c5e43a3db49f354603f9a5a905c3414a280c5b3144976e334c9130023af9a498');
INSERT INTO `ape_user_info` VALUES ('29', '13560478583', '13560478583', '13719207446', '25', '林六寿', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-18 18:20:42', '0', '390fa26eb96d72eb287b2e770ff3b4e4a64d91a34695b9dce7e9b2ff988fbd2e');
INSERT INTO `ape_user_info` VALUES ('30', '13829992279', '13829992279', '13926456855', '22', '胡兆宏', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '0', '0', '0.00', '0.00', '0.00', '2019-08-19 16:33:42', '0', 'd06b47952976eda0256ac98fa2589d67628b85439d0be071fee85614669216ca');

-- ----------------------------
-- Table structure for `ape_withdrawal_records`
-- ----------------------------
DROP TABLE IF EXISTS `ape_withdrawal_records`;
CREATE TABLE `ape_withdrawal_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `phone` varchar(15) NOT NULL COMMENT '手机号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `transfer_id` varchar(255) DEFAULT NULL COMMENT '支付宝账号',
  `to_name` varchar(20) DEFAULT NULL COMMENT '支付宝名称',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '金额',
  `servicecharge_amount` decimal(10,2) DEFAULT '0.00' COMMENT '手续费',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(11) DEFAULT '0' COMMENT '1审核中2提现成功3提现失败',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `withdrawal_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='提现';

-- ----------------------------
-- Records of ape_withdrawal_records
-- ----------------------------
INSERT INTO `ape_withdrawal_records` VALUES ('1', '15915840210', '3', '岁岁', null, 'cqy', 'cqy', '50.00', '0.00', '2019-08-14 21:36:26', '1');
INSERT INTO `ape_withdrawal_records` VALUES ('2', '18927564131', '4', null, '提现', '1122222223', '阿狗', '20.56', '3.00', '2019-08-16 22:36:45', '1');
INSERT INTO `ape_withdrawal_records` VALUES ('3', '18927564131', '4', null, '又提现', '44545456556', '阿猫', '100.50', '4.00', '2019-08-14 21:36:26', '2');

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', 'testTask', 'test', 'renren', '0 0/30 * * * ?', '0', '有参数测试', '2016-12-01 23:16:46');
INSERT INTO `schedule_job` VALUES ('2', 'testTask', 'test2', null, '0 0/30 * * * ?', '1', '无参数测试', '2016-12-03 14:55:56');

-- ----------------------------
-- Table structure for `schedule_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_captcha`
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统验证码';

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');
INSERT INTO `sys_config` VALUES ('2', 'SYS_WITHDRAW_RULE', '这是提现规则', '1', null);
INSERT INTO `sys_config` VALUES ('3', 'SYS_SHAREPAGE', 'https://b-ssl.duitang.com/uploads/item/201805/13/20180513224039_tgfwu.png', '1', null);
INSERT INTO `sys_config` VALUES ('4', 'SYS_ABOUTME', '这是关于我们', '1', null);
INSERT INTO `sys_config` VALUES ('5', 'PAY_ALINOTIFYURL', 'http://39.108.186.240:8082/catpay/app/pay/apilypayNotify', '1', '支付宝回调地址');
INSERT INTO `sys_config` VALUES ('6', 'USER_CHARGE', '20', '1', '提现手续费');
INSERT INTO `sys_config` VALUES ('7', 'USER_SHAREUSERURL', 'http://39.108.186.240:8082/catpay/static/reg/index.html?tzb=', '1', '二维码url');
INSERT INTO `sys_config` VALUES ('8', 'DefaultHeadImage', 'http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png', '1', '预定义头像地址');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[1]', '2', '0:0:0:0:0:0:0:1', '2019-08-12 22:12:03');
INSERT INTO `sys_log` VALUES ('2', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[1]', '0', '0:0:0:0:0:0:0:1', '2019-08-12 22:12:44');
INSERT INTO `sys_log` VALUES ('3', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":1,\"username\":null,\"email\":\"root@abc.io\",\"mobile\":\"13612345678\",\"status\":0,\"roleIdList\":[],\"createUserId\":null,\"createTime\":null}', '216', '0:0:0:0:0:0:0:1', '2019-08-12 22:17:54');
INSERT INTO `sys_log` VALUES ('4', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":1,\"username\":null,\"email\":\"root@abc.io\",\"mobile\":\"13612345678\",\"status\":0,\"roleIdList\":[],\"createUserId\":null,\"createTime\":null}', '2', '0:0:0:0:0:0:0:1', '2019-08-12 22:18:30');
INSERT INTO `sys_log` VALUES ('5', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":1,\"username\":null,\"email\":\"root@abc.io\",\"mobile\":\"13612345678\",\"status\":0,\"roleIdList\":[],\"createUserId\":null,\"createTime\":null}', '0', '0:0:0:0:0:0:0:1', '2019-08-12 22:21:30');
INSERT INTO `sys_log` VALUES ('6', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":1,\"username\":null,\"email\":\"root@abc.io\",\"mobile\":\"13612345678\",\"status\":0,\"roleIdList\":[],\"createUserId\":null,\"createTime\":null}', '0', '0:0:0:0:0:0:0:1', '2019-08-12 22:21:36');
INSERT INTO `sys_log` VALUES ('7', 'admin', '保存用户', 'org.catpay.modules.sys.controller.SysUserController.save()', '{\"userId\":2,\"username\":\"joyetety\",\"email\":\"adsfs@qq.com\",\"mobile\":\"13533846353\",\"status\":0,\"roleIdList\":[],\"createUserId\":1,\"createTime\":1565620302120}', '254', '0:0:0:0:0:0:0:1', '2019-08-12 22:31:42');
INSERT INTO `sys_log` VALUES ('8', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":2,\"username\":null,\"email\":\"adsfs@qq.com\",\"mobile\":\"13533846353\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":null}', '249', '0:0:0:0:0:0:0:1', '2019-08-12 22:32:01');
INSERT INTO `sys_log` VALUES ('9', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":2,\"username\":null,\"email\":\"adsfs@qq.com\",\"mobile\":\"13533846353\",\"status\":0,\"roleIdList\":[],\"createUserId\":1,\"createTime\":null}', '248', '0:0:0:0:0:0:0:1', '2019-08-12 22:33:24');
INSERT INTO `sys_log` VALUES ('10', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[2]', '62', '0:0:0:0:0:0:0:1', '2019-08-12 22:34:11');
INSERT INTO `sys_log` VALUES ('11', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[1]', '1', '0:0:0:0:0:0:0:1', '2019-08-12 22:34:27');
INSERT INTO `sys_log` VALUES ('12', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[1]', '0', '0:0:0:0:0:0:0:1', '2019-08-12 22:35:11');
INSERT INTO `sys_log` VALUES ('13', 'admin', '保存用户', 'org.catpay.modules.sys.controller.SysUserController.save()', '{\"userId\":3,\"username\":\"joyetety\",\"email\":\"sdfsadf@qwe.com\",\"mobile\":\"12345678912\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":1565620548391}', '252', '0:0:0:0:0:0:0:1', '2019-08-12 22:35:49');
INSERT INTO `sys_log` VALUES ('14', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[3]', '61', '0:0:0:0:0:0:0:1', '2019-08-12 22:36:00');
INSERT INTO `sys_log` VALUES ('15', 'admin', '保存用户', 'org.catpay.modules.sys.controller.SysUserController.save()', '{\"userId\":4,\"username\":\"joyetety\",\"email\":\"jdsklf@dsf.com\",\"mobile\":\"12345678901\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":1565620610057}', '805', '0:0:0:0:0:0:0:1', '2019-08-12 22:36:50');
INSERT INTO `sys_log` VALUES ('16', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[4]', '64', '0:0:0:0:0:0:0:1', '2019-08-12 22:37:47');
INSERT INTO `sys_log` VALUES ('17', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":1,\"username\":null,\"email\":\"root@abc.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[],\"createUserId\":null,\"createTime\":null}', '0', '0:0:0:0:0:0:0:1', '2019-08-12 22:38:54');
INSERT INTO `sys_log` VALUES ('18', 'admin', '保存用户', 'org.catpay.modules.sys.controller.SysUserController.save()', '{\"userId\":5,\"username\":\"joyetety\",\"email\":\"sdf@qq.com\",\"mobile\":\"12345678901\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":1565620753403}', '244', '0:0:0:0:0:0:0:1', '2019-08-12 22:39:14');
INSERT INTO `sys_log` VALUES ('19', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":5,\"username\":null,\"email\":\"sdf@qq.com\",\"mobile\":\"12345673432\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":null}', '669', '0:0:0:0:0:0:0:1', '2019-08-12 22:39:37');
INSERT INTO `sys_log` VALUES ('20', 'admin', '保存用户', 'org.catpay.modules.sys.controller.SysUserController.save()', '{\"userId\":6,\"username\":\"aaa\",\"email\":\"sdfa@qq.com\",\"mobile\":\"13533556356\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":1565620885378}', '243', '0:0:0:0:0:0:0:1', '2019-08-12 22:41:26');
INSERT INTO `sys_log` VALUES ('21', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[5]', '61', '0:0:0:0:0:0:0:1', '2019-08-12 22:41:33');
INSERT INTO `sys_log` VALUES ('22', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":6,\"username\":null,\"email\":\"sdfa@qq.com\",\"mobile\":\"13533556356\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":null}', '241', '0:0:0:0:0:0:0:1', '2019-08-12 22:43:44');
INSERT INTO `sys_log` VALUES ('23', 'admin', '修改用户', 'org.catpay.modules.sys.controller.SysUserController.update()', '{\"userId\":6,\"username\":null,\"email\":\"sdfa@qq.com\",\"mobile\":\"13533556356\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":null}', '581', '0:0:0:0:0:0:0:1', '2019-08-12 22:43:58');
INSERT INTO `sys_log` VALUES ('24', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '5', '0', '127.0.0.1', '2019-08-13 19:18:25');
INSERT INTO `sys_log` VALUES ('25', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '6', '0', '127.0.0.1', '2019-08-13 19:18:29');
INSERT INTO `sys_log` VALUES ('26', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '27', '0', '127.0.0.1', '2019-08-13 19:18:32');
INSERT INTO `sys_log` VALUES ('27', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '7', '0', '127.0.0.1', '2019-08-14 18:41:00');
INSERT INTO `sys_log` VALUES ('28', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '7', '282', '127.0.0.1', '2019-08-14 18:43:00');
INSERT INTO `sys_log` VALUES ('29', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":36,\"parentId\":0,\"parentName\":\"一级菜单\",\"name\":\"订单管理\",\"url\":\"business/orderList\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":7,\"open\":null,\"list\":null}', '193', '127.0.0.1', '2019-08-14 18:45:24');
INSERT INTO `sys_log` VALUES ('30', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '36', '281', '127.0.0.1', '2019-08-14 18:45:41');
INSERT INTO `sys_log` VALUES ('31', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":37,\"parentId\":1,\"parentName\":\"系统管理\",\"name\":\"订单管理\",\"url\":\"business/orderList\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":7,\"open\":null,\"list\":null}', '617', '127.0.0.1', '2019-08-14 18:45:56');
INSERT INTO `sys_log` VALUES ('32', 'admin', '修改角色', 'org.catpay.modules.sys.controller.SysRoleController.update()', '{\"roleId\":1,\"roleName\":null,\"remark\":\"管理员\",\"createUserId\":1,\"menuIdList\":[1,3,19,20,21,22,6,37],\"createTime\":null}', '805', '127.0.0.1', '2019-08-14 18:52:49');
INSERT INTO `sys_log` VALUES ('33', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":38,\"parentId\":1,\"parentName\":\"系统管理\",\"name\":\"参数管理\",\"url\":\"sys/configList\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":8,\"open\":null,\"list\":null}', '541', '0:0:0:0:0:0:0:1', '2019-08-15 11:07:58');
INSERT INTO `sys_log` VALUES ('34', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":39,\"parentId\":38,\"parentName\":\"参数管理\",\"name\":\"查看\",\"url\":\"\",\"perms\":\"\",\"type\":2,\"icon\":\"\",\"orderNum\":0,\"open\":null,\"list\":null}', '266', '0:0:0:0:0:0:0:1', '2019-08-15 12:00:25');
INSERT INTO `sys_log` VALUES ('35', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '39', '1533', '0:0:0:0:0:0:0:1', '2019-08-15 12:04:48');
INSERT INTO `sys_log` VALUES ('36', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":40,\"parentId\":38,\"parentName\":\"参数管理\",\"name\":\"查看\",\"url\":\"\",\"perms\":\"\",\"type\":2,\"icon\":\"sys:config:list\",\"orderNum\":0,\"open\":null,\"list\":null}', '494', '0:0:0:0:0:0:0:1', '2019-08-15 12:05:21');
INSERT INTO `sys_log` VALUES ('37', 'admin', '保存配置', 'org.catpay.modules.sys.controller.SysConfigController.save()', '{\"id\":9,\"paramKey\":\"CESHI\",\"paramValue\":\"ceshicanshu\",\"remark\":\"123\"}', '148', '0:0:0:0:0:0:0:1', '2019-08-15 15:33:48');
INSERT INTO `sys_log` VALUES ('38', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '40', '322', '0:0:0:0:0:0:0:1', '2019-08-15 15:34:43');
INSERT INTO `sys_log` VALUES ('39', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":9,\"paramKey\":\"CESHI12\",\"paramValue\":\"ceshicanshu12\",\"remark\":\"456789\"}', '287', '0:0:0:0:0:0:0:1', '2019-08-15 16:18:13');
INSERT INTO `sys_log` VALUES ('40', 'admin', '删除配置', 'org.catpay.modules.sys.controller.SysConfigController.delete()', '[9]', '250', '0:0:0:0:0:0:0:1', '2019-08-15 16:18:26');
INSERT INTO `sys_log` VALUES ('41', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":41,\"parentId\":1,\"parentName\":\"系统管理\",\"name\":\"奖励规则管理\",\"url\":\"business/ruleList\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0,\"open\":null,\"list\":null}', '252', '0:0:0:0:0:0:0:1', '2019-08-16 11:19:49');
INSERT INTO `sys_log` VALUES ('42', 'admin', '删除菜单', 'org.catpay.modules.sys.controller.SysMenuController.delete()', '41', '300', '0:0:0:0:0:0:0:1', '2019-08-16 11:20:31');
INSERT INTO `sys_log` VALUES ('43', 'admin', '保存菜单', 'org.catpay.modules.sys.controller.SysMenuController.save()', '{\"menuId\":42,\"parentId\":1,\"parentName\":\"系统管理\",\"name\":\"奖励规则管理\",\"url\":\"business/ruleList\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":9,\"open\":null,\"list\":null}', '208', '0:0:0:0:0:0:0:1', '2019-08-16 11:20:48');
INSERT INTO `sys_log` VALUES ('44', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":5,\"paramKey\":\"PAY_ALINOTIFYURL\",\"paramValue\":\"http://39.108.186.240:8082/catpay/app/pay/apilypayNotify\",\"remark\":\"支付宝回调地址\"}', '30', '121.32.176.20', '2019-08-16 23:47:42');
INSERT INTO `sys_log` VALUES ('45', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":8,\"paramKey\":\"DefaultHeadImage\",\"paramValue\":\"https://b-ssl.duitang.com/uploads/item/201805/13/20180513224039_tgfwu.png\",\"remark\":\"预定义头像地址\"}', '19', '121.32.176.20', '2019-08-16 23:48:40');
INSERT INTO `sys_log` VALUES ('46', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":6,\"paramKey\":\"USER_CHARGE\",\"paramValue\":\"20\",\"remark\":\"提现手续费\"}', '16', '121.32.176.20', '2019-08-16 23:50:10');
INSERT INTO `sys_log` VALUES ('47', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":7,\"paramKey\":\"USER_SHAREUSERURL\",\"paramValue\":\"http://img.zcool.cn/community/012c5c561c794632f8755701de44ab.jpg\",\"remark\":\"二维码url\"}', '18', '121.32.176.20', '2019-08-16 23:51:01');
INSERT INTO `sys_log` VALUES ('48', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":7,\"paramKey\":\"USER_SHAREUSERURL\",\"paramValue\":\"http://http://39.108.186.240:8082/catpay/static/reg/index.html?tzb=\",\"remark\":\"二维码url\"}', '18', '121.32.176.20', '2019-08-17 01:54:01');
INSERT INTO `sys_log` VALUES ('49', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":7,\"paramKey\":\"USER_SHAREUSERURL\",\"paramValue\":\"http://39.108.186.240:8082/catpay/static/reg/index.html?tzb=\",\"remark\":\"二维码url\"}', '33', '121.32.176.20', '2019-08-17 02:52:19');
INSERT INTO `sys_log` VALUES ('50', 'admin', '停用用户', 'org.catpay.modules.sys.controller.SysUserController.unable()', '{\"userId\":11,\"username\":null,\"email\":null,\"mobile\":null,\"status\":0,\"roleIdList\":null,\"createUserId\":null,\"createTime\":null}', '24', '119.131.89.120', '2019-08-17 09:14:39');
INSERT INTO `sys_log` VALUES ('51', 'admin', '删除用户', 'org.catpay.modules.sys.controller.SysUserController.delete()', '[11]', '15', '119.131.89.120', '2019-08-17 09:14:44');
INSERT INTO `sys_log` VALUES ('52', 'admin', '修改配置', 'org.catpay.modules.sys.controller.SysConfigController.update()', '{\"id\":8,\"paramKey\":\"DefaultHeadImage\",\"paramValue\":\"http://image.yidongtuozhan.com/pro_85eb2ecf6cc14681811fb03ec0f253d1.png\",\"remark\":\"预定义头像地址\"}', '34', '119.131.89.120', '2019-08-17 15:48:24');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', null, '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '管理员列表', 'sys/userList', null, '1', null, '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'sys/roleList', null, '1', null, '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'sys/menuList', null, '1', null, '3');
INSERT INTO `sys_menu` VALUES ('5', '1', '商品管理', 'business/productList', null, '1', null, '4');
INSERT INTO `sys_menu` VALUES ('6', '1', '商户管理', 'business/userList', null, '1', null, '5');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('37', '1', '订单管理', 'business/orderList', null, '1', null, '7');
INSERT INTO `sys_menu` VALUES ('38', '1', '参数管理', 'sys/configList', null, '1', null, '8');
INSERT INTO `sys_menu` VALUES ('42', '1', '奖励规则管理', 'business/ruleList', null, '1', null, '9');

-- ----------------------------
-- Table structure for `sys_oss`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', '管理员', '1', '2018-09-06 21:14:29');
INSERT INTO `sys_role` VALUES ('5', '城市地方', '范德萨', '8', '2018-09-06 21:22:06');
INSERT INTO `sys_role` VALUES ('7', 'ssss', 'sss', '9', '2018-09-06 21:24:41');
INSERT INTO `sys_role` VALUES ('9', '放松放松', '佛挡杀佛', '9', '2018-09-06 21:33:17');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('48', '3', '2');
INSERT INTO `sys_role_menu` VALUES ('49', '3', '15');
INSERT INTO `sys_role_menu` VALUES ('50', '3', '16');
INSERT INTO `sys_role_menu` VALUES ('51', '3', '17');
INSERT INTO `sys_role_menu` VALUES ('52', '3', '18');
INSERT INTO `sys_role_menu` VALUES ('53', '3', '3');
INSERT INTO `sys_role_menu` VALUES ('54', '3', '19');
INSERT INTO `sys_role_menu` VALUES ('55', '3', '20');
INSERT INTO `sys_role_menu` VALUES ('56', '3', '21');
INSERT INTO `sys_role_menu` VALUES ('57', '3', '22');
INSERT INTO `sys_role_menu` VALUES ('58', '3', '4');
INSERT INTO `sys_role_menu` VALUES ('59', '3', '23');
INSERT INTO `sys_role_menu` VALUES ('60', '3', '24');
INSERT INTO `sys_role_menu` VALUES ('61', '3', '25');
INSERT INTO `sys_role_menu` VALUES ('62', '3', '26');
INSERT INTO `sys_role_menu` VALUES ('63', '3', '-666666');
INSERT INTO `sys_role_menu` VALUES ('64', '3', '1');
INSERT INTO `sys_role_menu` VALUES ('72', '5', '3');
INSERT INTO `sys_role_menu` VALUES ('73', '5', '19');
INSERT INTO `sys_role_menu` VALUES ('74', '5', '20');
INSERT INTO `sys_role_menu` VALUES ('75', '5', '21');
INSERT INTO `sys_role_menu` VALUES ('76', '5', '22');
INSERT INTO `sys_role_menu` VALUES ('77', '5', '4');
INSERT INTO `sys_role_menu` VALUES ('78', '5', '23');
INSERT INTO `sys_role_menu` VALUES ('79', '5', '24');
INSERT INTO `sys_role_menu` VALUES ('80', '5', '25');
INSERT INTO `sys_role_menu` VALUES ('81', '5', '26');
INSERT INTO `sys_role_menu` VALUES ('82', '5', '-666666');
INSERT INTO `sys_role_menu` VALUES ('83', '5', '1');
INSERT INTO `sys_role_menu` VALUES ('107', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('108', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('109', '1', '19');
INSERT INTO `sys_role_menu` VALUES ('110', '1', '20');
INSERT INTO `sys_role_menu` VALUES ('111', '1', '21');
INSERT INTO `sys_role_menu` VALUES ('112', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('113', '1', '6');
INSERT INTO `sys_role_menu` VALUES ('114', '1', '37');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `profile_photo` varchar(128) DEFAULT NULL,
  `role_list` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '02a3e0ca22f11691c0dd1421d795e9849a0e2cc0e49f79d7b09c9dbe3ec10679', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', '1', '1', '2016-11-11 11:11:11', null, null);
INSERT INTO `sys_user` VALUES ('8', 'admin2', 'b6c3718119db6cc2a5be8859b20323733fe6288fc3a430d6260601b60c78d411', 'YzcmCZNvbXocrsz9dm8e', 'dsf@fas.com', '13533846734', '1', '1', '2018-09-06 21:19:11', null, '管理员,城市地方,ssss');
INSERT INTO `sys_user` VALUES ('9', 'admin3', '2e4f6b16638abd76eba32da056858a466b4329f3bb11873fd9aa4903d75d2d79', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '1', '2018-09-06 21:24:05', null, '管理员');
INSERT INTO `sys_user` VALUES ('12', 'admin5', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('13', 'admin6', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('14', 'admin7', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('15', 'admin8', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('16', 'admin9', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('18', 'admin11', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '1', '2018-09-06 21:24:05', null, '管理员,城市地方,ssss');
INSERT INTO `sys_user` VALUES ('19', 'admin12', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('20', 'admin13', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('21', 'admin14', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('22', 'admin15', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);
INSERT INTO `sys_user` VALUES ('23', 'admin16', 'df164524fd63675aa9614fec56b326799793c5797e0d0d6080e5fc072c1b3821', 'tXUK2VohHs09z1jug4MF', 'dsfkjsadkf@sdaf.com', '13533846734', '1', '8', '2018-09-06 21:24:05', null, null);

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('74', '18', '1');
INSERT INTO `sys_user_role` VALUES ('75', '18', '5');
INSERT INTO `sys_user_role` VALUES ('76', '18', '7');
INSERT INTO `sys_user_role` VALUES ('77', '24', '1');
INSERT INTO `sys_user_role` VALUES ('78', '24', '5');
INSERT INTO `sys_user_role` VALUES ('79', '24', '7');
INSERT INTO `sys_user_role` VALUES ('80', '24', '9');
INSERT INTO `sys_user_role` VALUES ('85', '9', '1');
INSERT INTO `sys_user_role` VALUES ('88', '8', '1');
INSERT INTO `sys_user_role` VALUES ('89', '8', '5');
INSERT INTO `sys_user_role` VALUES ('90', '8', '7');

-- ----------------------------
-- Table structure for `sys_user_token`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户Token';

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');
